### Dependencies

* Python 3.6 with the following libraries: numpy, scipy, matplotlib, pandas, psycopg2, xlsxwriter.

  Recommended to use Anaconda 5.1+

* PostgreSQL Server 10 with PostGIS
* QGIS or software that can import shapefiles to PostGIS

### Instructions

1. Unpack the `large_data.zip` archive into the root folder

2. Unpack `spatial_data.zip` archive and import the shapefiles into the PSQL server using QGIS:

   * `asiras_footprints.shp` to table `asr_ftpr`

   * `ice_roughness.shp` to table `isr_optm`

3. Run python scripts in order

   1. `step_prep_data.py`
   2. `step_analyze_offset.py`
   3. `step_calc_error.py`
   4. `step_analyze_corr.py`







