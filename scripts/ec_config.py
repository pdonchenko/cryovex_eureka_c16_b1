"""
Configuration for eureka-cryovex analysis.
Author: Paul Donchenko - University of Waterloo
"""

# login details to connect to pgsql database
# dbname and password placeholders must be replaced with local database
# details
pg_login = {
    'host' : 'localhost',
    'user' : 'postgres',
    'password' : 'password',
    'dbname' : 'cveureka'
}

# defaults
d_schema = 'public'

# source data paths
als_path = r"../data/l1b/ALS_L1B_20140325T160930_164957"
asr_path = r"..\data\l1b\AS3OA03_ASIWL1B040320140325T160941_20140325T16423" \
             r"3_0001.DBL"
erk_path = r"../data/eureka/ECCCEureka2014.h5"

# database fields/column names
# NOTE: this is hardcoded into most queries for convenience
col_geom = 'geom'

# analysis configuration
optm_dist = 40
max_roll_dev = 1.5 # roll and pitch based on ASIRAS report pg 30
max_pitch_dev = 1.5
asp_config = (0.05, 0.95) # low, high for asperity percentages (h-topo)
dist_config = (2, 40, 2) # min, max, step for distance ranges
round_places = 4
significance = 0.01

# database literals
src_srid = 4326 # all source data in WGS84 GCS
tgt_srid = 5921 # write to local EPSG Arctic Regional zone A1 (WGS84 datum)

# database tables
# extracted source data
als_src = 'als_src'
asr_src = 'asr_src'

# individual eureka dataset
auger_src = 'auger_src'
esc30_src = 'esc30_src'
mgn_src = 'mgn_src'

# combine eureka dataset
erk_src = (auger_src, esc30_src, mgn_src)

# optimal filtered data
asr_optm = 'asr_optm' # filtered asr nadirs
asr_optm_bufr = 'asr_optm_bufr' # buffer around each nadir
asr_optm_zone = 'asr_optm_zone' # single feature buffer around all nadirs
# features filtered to optimal asiras
mgn_optm = 'mgn_optm' # magnaprobe snow depth
als_optm = 'als_optm' # ALS snow surface elevation
isr_optm = 'isr_optm' # ise snow roughness, created manually in QGIS

# asiras footprints
asr_ftpr = 'asr_ftpr'
asr_ftpr_optm = 'asr_ftpr_optm'

# snow density dataset creation
asr_snow_dens = 'asr_snow_dens' # nadirs snow density table
snow_dens_index_col = 'mgn_oid'

# ise snow elevation creation
ise = 'ise' # manually created using ArcGIS

# ice surface roughness nearest to ASIRAS nadirs
# this is to fix footprints not having enough coverage to always
# find a nearby isr point
isr_near = 'isr_near'

# spatial aggregation
agr_mgn = 'agr_mgn'
agr_als = 'agr_als'
agr_ise = 'agr_ise'
agr_isr = 'agr_isr'
agr_all = 'agr_all'

prefix_mgn = 'mgn'
prefix_als = 'als'
prefix_ise = 'ise'

valcol_mgn = 'snow_depth'
valcol_als = 'snow_elvtn'
valcol_ise = 'ice_elvtn'

# footprint spatial aggregation
ftagr_mgn = 'ftagr_mgn'
ftagr_als = 'ftagr_als'
ftagr_ise = 'ftagr_ise'
ftagr_isr = 'ftagr_isr'
ftagr_all = 'ftagr_all'

# distance marker for footprint aggregation
# does not interfere with other distance ranges
ft_dist = -1
# complete aggregation
agr_full = 'agr_full'

agr_surf = 'agr_surf'

# waveform characteristics
asr_elvtn = 'asr_elvtn'
asr_char = 'asr_char'
asr_wvfm_char = 'asr_wvfm_char'

# size of each ASIRAS bin in meters (pg 59 in data products description)
bin_size = 0.109787
# percent of peak at which the return is considered to be bound
signal_threshold = 0.01

# calibration
quality_colors = {'best':'blue','high':'green','med':'orange'}
quality_means = (('best',),('best','high'),('best','high','med'))

# error calculation
als_asr_offset = 3.80
error_full = 'error_full'
error_ft = 'error_ft'
error_dist = 'error_dist'

# combining features
comb_ft = 'comb_ft'
comb_dist = 'comb_dist'
comb_char = 'comb_char'

# checking for normal distribution
normal_thresh = 0.05
unique_thresh = 10



