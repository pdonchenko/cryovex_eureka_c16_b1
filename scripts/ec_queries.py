"""
Stores queries for using functions in the eureka-cryovex analysis.
Author: Paul Donchenko - University of Waterloo
"""

# CALCULATING OPTIMAL TRACK
asr_optm = \
"""
CREATE TABLE {output} AS (
WITH _valid AS (
        SELECT oid_asr,heading,roll,pitch,surface_elvtn,geom
        FROM {asr_src}
        WHERE roll >= -{roll_dev} AND roll <= {roll_dev}
        AND pitch >= -{pitch_dev} AND pitch <= {pitch_dev}
    )
SELECT DISTINCT asr.*
FROM _valid asr
    INNER JOIN {mgn_src} mgn
    ON st_dwithin(asr.geom, mgn.geom, {dist})
)
"""

asr_optm_bufr = \
"""
CREATE TABLE {output} AS (
SELECT oid_asr, st_buffer(geom, {dist}) geom
FROM {asr_optm}
)
"""

asr_optm_zone = \
"""
CREATE TABLE {output} AS (
SELECT st_multi(st_union(geom)) geom FROM {asr_optm_bufr}
)
"""

# apparently it is faster to generate the unioned buffer locally than
# to read it from an existing table
optm_obsv = \
"""
CREATE TABLE {output} AS (
WITH
    _buffers AS ( SELECT st_buffer(geom, 40) geom FROM {asr_optm}),
    _area AS (SELECT st_multi(st_union(geom)) geom FROM _buffers)
SELECT src.*
FROM {source} AS src JOIN _area
ON st_intersects(src.geom, _area.geom)
)
"""

# CREATE INTERMEDIATE DATASETS
snow_dens = \
"""
-- create temporary table of nearest esc30 to each ASIRAS
-- this is required because of an issue in WITH queries making them
-- unoptimized for this comparison
DROP TABLE IF EXISTS temp_snow_dens_nearest;
CREATE TABLE temp_snow_dens_nearest AS (
		SELECT asr.oid_asr, _near.snow_dens snow_dens_nearest
		FROM {asr_src} asr
			JOIN LATERAL (
				SELECT asr.oid_asr, (snow_rho1+snow_rho2)/2 snow_dens,
				    dens.geom
				FROM {esc30_src} dens
				ORDER BY asr.geom <-> dens.geom LIMIT 1
			) AS _near USING (oid_asr)
		ORDER BY asr.oid_asr
);
-- create temporary table of distance-interpolate snow density from two nearest
-- esc30 points to each ASIRAS
DROP TABLE IF EXISTS temp_snow_dens_interp;
CREATE TABLE temp_snow_dens_interp AS (
WITH
	_pairs AS (
		SELECT asr.oid_asr, _near.snow_dens,
		    st_distance(asr.geom, _near.geom) dist
		FROM {asr_src} asr
			JOIN LATERAL (
				SELECT asr.oid_asr, (snow_rho1+snow_rho2)/2 snow_dens,
				    dens.geom
				FROM {esc30_src} dens
				ORDER BY asr.geom <-> dens.geom LIMIT 2
			) AS _near USING (oid_asr)
		ORDER BY asr.oid_asr
	),
	_dist_sums AS (
		SELECT oid_asr, SUM(dist)  dist_sum
		FROM _pairs GROUP BY oid_asr
		ORDER BY oid_asr
	),
	_scaled AS (
		SELECT oid_asr, snow_dens,
			snow_dens*(dist/dist_sum) snow_dens_scaled,
			dist
		FROM _pairs JOIN _dist_sums USING (oid_asr)
	)
	SELECT oid_asr,
		AVG(snow_dens) snow_dens_avg,
		SUM(snow_dens_scaled) snow_dens_interp,
		MIN(dist) dist_near,
		MAX(dist) dist_far
	FROM _scaled
	GROUP BY oid_asr ORDER BY oid_asr
);
-- combine the nearest and interpolated snow densities
CREATE TABLE {output} AS (
	SELECT *
	FROM temp_snow_dens_nearest
	JOIN temp_snow_dens_interp USING (oid_asr)
);
-- drop the temporary tables
DROP TABLE temp_snow_dens_interp;
DROP TABLE temp_snow_dens_nearest;
"""

linked_ice_elvtn =\
"""
CREATE TABLE {output} AS (
SELECT oid_mgn, mgn.snow_depth, _near.snow_elvtn "als_elvtn",
    _near.snow_elvtn-mgn.snow_depth "ice_elvtn",
    st_distance(mgn.geom, _near.geom) "als_dist", mgn.geom
FROM {mgn_src} mgn
JOIN LATERAL (
    SELECT mgn.oid_mgn, als.snow_elvtn, als.geom
    FROM {als_src} als
    ORDER BY mgn.geom <-> als.geom
    LIMIT 1
    ) AS _near USING (oid_mgn)
)
"""

# SPATIAL AGGREGATION

agr_isr = \
"""
CREATE TABLE {output} AS (
WITH
	_near AS (
		SELECT _local_near.*
		FROM {asr_optm} asr
		JOIN LATERAL (
			SELECT asr.oid_asr, isr.roughness ice_rough_near
			FROM {isr_optm} isr
			ORDER BY asr.geom <-> isr.geom
			LIMIT 1
			) AS _local_near USING (oid_asr)
	),
	_dists AS (
		SELECT generate_series({dist_min},{dist_max},{dist_step}) dist
	),
	_agr AS (
		SELECT nadir.oid_asr, dist, MIN(roughness) ice_rough_min,
			MAX(roughness) ice_rough_max,
			ROUND(AVG(roughness),{round_places}) ice_rough_perc
		FROM {isr_optm} isr, {asr_optm} nadir, {asr_optm_bufr} bufr, _dists
		WHERE nadir.oid_asr = bufr.oid_asr
			AND st_contains(bufr.geom, isr.geom)
			AND st_dwithin(nadir.geom, isr.geom, dist)
		GROUP BY nadir.oid_asr, dist
	)
SELECT * FROM _agr JOIN _near USING (oid_asr)
ORDER BY dist,oid_asr
)
"""

ftagr_isr = \
"""
CREATE TABLE {output} AS (
WITH
	_near AS (
		SELECT _local_near.*
		FROM {asr_optm} asr
		JOIN LATERAL (
			SELECT asr.oid_asr, isr.roughness ice_rough_near
			FROM {isr_optm} isr
			ORDER BY asr.geom <-> isr.geom
			LIMIT 1
			) AS _local_near USING (oid_asr)
	),
	_optm_ftpr AS (
        SELECT ftpr.*
        FROM {asr_ftpr} ftpr, {asr_optm} optm
        WHERE ftpr.oid_asr = optm.oid_asr
    ),
    _ftagr AS (
		SELECT ftpr.oid_asr, MIN(roughness) ice_rough_min,
			MAX(roughness) ice_rough_max,
			ROUND(AVG(roughness),{round_places}) ice_rough_perc
		FROM {isr_optm} isr, _optm_ftpr ftpr
		WHERE st_contains(ftpr.geom, isr.geom)
		GROUP BY ftpr.oid_asr
	)
SELECT * FROM _ftagr
    FULL OUTER JOIN _near USING (oid_asr)
ORDER BY oid_asr
)
"""

agr_dist = \
"""
CREATE TABLE {output} AS (
WITH _dists AS (
    SELECT generate_series({dist_min},{dist_max},{dist_step}) "dist"
)
SELECT
    nadir.oid_asr,
    _dists.dist "dist",
    COUNT(*) "{prefix}_count",
    AVG(obsv.{value}) "{prefix}_mean",
    MIN(obsv.{value}) "{prefix}_min",
    MAX(obsv.{value}) "{prefix}_max",
    MAX(obsv.{value}) - MIN(obsv.{value}) "{prefix}_range",
    VAR_SAMP(obsv.{value}) "{prefix}_var",
    PERCENTILE_CONT({pr_top})
        WITHIN GROUP (ORDER BY obsv.{value} ASC)
        - PERCENTILE_CONT({pr_bottom})
            WITHIN GROUP (ORDER BY obsv.{value}) "{prefix}_asperity_cont",
    PERCENTILE_DISC({pr_top})
        WITHIN GROUP (ORDER BY obsv.{value} ASC)
        - PERCENTILE_CONT({pr_bottom})
            WITHIN GROUP (ORDER BY obsv.{value}) "{prefix}_asperity_disc"
            
FROM {observ} AS obsv, {nadir} as nadir, {buffer} AS bufr, _dists
WHERE nadir.oid_asr = bufr.oid_asr
AND ST_CONTAINS(bufr.geom, obsv.geom)
AND ST_DWITHIN(nadir.geom, obsv.geom, _dists.dist)
GROUP BY nadir.oid_asr, _dists.dist
)
"""

agr_combine_dist = \
"""
CREATE TABLE {output} AS (
WITH asr AS (
        SELECT
            oid_asr,
            generate_series({dist_min},{dist_max},{dist_step}) "dist"
        FROM {asr_optm}
    )
SELECT *
FROM asr
    FULL OUTER JOIN {agr_mgn} USING (oid_asr, dist)
    FULL OUTER JOIN {agr_als} USING (oid_asr, dist)
    FULL OUTER JOIN {agr_ise} USING (oid_asr, dist)
    FULL OUTER JOIN {agr_isr} USING (oid_asr, dist)
ORDER BY (oid_asr, dist)
)
"""

create_optm_ftpr = \
"""
CREATE TABLE {output} AS (
    SELECT ftpr.*
    FROM {asr_ftpr} ftpr, {asr_optm} optm
    WHERE ftpr.oid_asr = optm.oid_asr
)
"""

agr_footprint = \
"""
CREATE TABLE {output} AS (
WITH _optm_ftpr AS (
    SELECT ftpr.*
    FROM {asr_ftpr} ftpr, {asr_optm} optm
    WHERE ftpr.oid_asr = optm.oid_asr
)
SELECT
    ftpr.oid_asr,
    COUNT(*) "{prefix}_count",
    AVG(obsv.{value}) "{prefix}_mean",
    MIN(obsv.{value}) "{prefix}_min",
    MAX(obsv.{value}) "{prefix}_max",
    MAX(obsv.{value}) - MIN(obsv.{value}) "{prefix}_range",
    VAR_SAMP(obsv.{value}) "{prefix}_var",
    PERCENTILE_CONT({pr_top})
        WITHIN GROUP (ORDER BY obsv.{value} ASC)
        - PERCENTILE_CONT({pr_bottom})
            WITHIN GROUP (ORDER BY obsv.{value}) "{prefix}_asperity_cont",
    PERCENTILE_DISC({pr_top})
        WITHIN GROUP (ORDER BY obsv.{value} ASC)
        - PERCENTILE_CONT({pr_bottom})
            WITHIN GROUP (ORDER BY obsv.{value}) "{prefix}_asperity_disc"
            
FROM {observ} AS obsv, _optm_ftpr AS ftpr
WHERE ST_CONTAINS(ftpr.geom, obsv.geom)
GROUP BY ftpr.oid_asr
)
"""

agr_combine_footprint = \
"""
CREATE TABLE {output} AS (
WITH
    _asr AS (SELECT oid_asr FROM {asr_optm})
SELECT *
FROM _asr
    FULL OUTER JOIN {ftagr_mgn} USING (oid_asr)
    FULL OUTER JOIN {ftagr_als} USING (oid_asr)
    FULL OUTER JOIN {ftagr_ise} USING (oid_asr)
    FULL OUTER JOIN {ftagr_isr} USING (oid_asr)
ORDER BY oid_asr
)
"""

agr_combine_full = \
"""
CREATE TABLE {output} AS (
WITH
    -- VERY HACKY: bumps the oid column by joining to a subset of itself
    -- which only has that column
	_leftcols AS ( SELECT oid_asr, {ft_dist} dist FROM {ftagr_all} ),
	_combined AS ( SELECT * FROM _leftcols JOIN {ftagr_all} USING (oid_asr) )
SELECT * FROM _combined
UNION
SELECT * FROM {agr_all}
ORDER BY dist, oid_asr
)
"""

agr_surface = \
"""
CREATE TABLE {output} AS (
(
SELECT
	oid_asr, dist, 'spread' surf_calc, als_mean snow_elvtn,
	mgn_mean snow_depth, als_mean-mgn_mean ice_elvtn
FROM {agr_full}
WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
UNION
SELECT
	oid_asr, dist, 'linked' surf_calc, als_mean snow_elvtn,
	mgn_mean snow_depth, ise_mean ice_elvtn
FROM {agr_full}
WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
AND ise_mean IS NOT NULL
)
ORDER BY oid_asr,dist,surf_calc
)
"""

# ANALYZING OFFSETS

analyze_offsets = \
"""
WITH
    _offsets AS (
        SELECT * FROM ( VALUES {values_list} ) s(offset_name,offset_val)
    ),
    _surf AS (
        SELECT oid_asr, als_mean snow_elvtn, mgn_mean snow_depth,
            ise_mean ice_elvtn
        FROM {ftagr_all}
        WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
    ),
    _sensor AS (
        SELECT oid_asr, offset_Name, offset_val,
            tfmra_elvtn+offset_val est_elvtn
        FROM {asr_elvtn}, _offsets
        WHERE tfmra_level={tfmra_threshold}
    )
SELECT offset_Name, offset_val,
    SUM((est_elvtn>snow_elvtn)::integer) "above_snow_spread",
    SUM((est_elvtn>(ice_elvtn+snow_depth))::integer) "above_snow_linked",
    SUM((est_elvtn<(snow_elvtn-snow_depth))::integer) "below_ice_spread",
    SUM((est_elvtn<(ice_elvtn))::integer) "below_ice_linked",
    |/(SUM(((est_elvtn-(snow_elvtn-snow_depth))^2))/COUNT(*)) "rmse_spread",
    |/(SUM(((est_elvtn-ice_elvtn)^2))/COUNT(*)) "rmse_linked",
    CORR(est_elvtn,(snow_elvtn-snow_depth)) "corr_spread",
    CORR(est_elvtn,ice_elvtn) "corr_linked"
    
FROM _sensor JOIN _surf USING (oid_asr)
GROUP BY offset_Name, offset_val
ORDER BY "rmse_spread"
"""

# TODO fix this query to fit asr_elvtn
analyze_offset_range =\
"""
WITH
    -- generate a series of offsets between two values
    _offsets AS (
        SELECT generate_series({offset_min},{offset_max},{offset_step})
        AS offset_val
    ),
    -- estimate the snow surface properties from observations
    -- but only when all observations are within the observation radius or
    -- footprint
    _surf AS (
        SELECT oid_asr, als_mean snow_elvtn, mgn_mean snow_depth,
            ise_mean ice_elvtn
        FROM {ftagr_all}
        WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
    ),
    -- apply every offset to a selected tfmra threshold elevation
    _sensor AS (
        SELECT oid_asr, offset_val, tfmra_elvtn+offset_val est_elvtn
        FROM {asr_elvtn}, _offsets
        WHERE tfmra_level={tfmra_threshold}
    )
SELECT offset_val,
    |/(SUM(((est_elvtn-(snow_elvtn-snow_depth))^2))/COUNT(*)) "rmse_spread",
    |/(SUM(((est_elvtn-ice_elvtn)^2))/COUNT(*)) "rmse_linked"
FROM _sensor JOIN _surf USING (oid_asr)
GROUP BY offset_val
ORDER BY offset_val
"""

# CALCULATING ERROR

error_full = \
"""
CREATE TABLE {output} AS (
WITH
    -- list of hand-picked offsets as a table to mix with observation data
	_offsets AS (
		SELECT *
		FROM ( VALUES {values_list} ) s(offset_name, offset_val)
	),
	-- apply every offset to every combination of estimated tfmra elevations
	_sensor AS (
		SELECT oid_asr, tfmra_level, offset_name, offset_val,
			tfmra_elvtn+offset_val adj_elvtn
		FROM {asr_elvtn}, _offsets
	)
SELECT
	oid_asr, dist, surf_calc, tfmra_level, offset_name, offset_val, adj_elvtn,
	adj_elvtn-ice_elvtn error, @(adj_elvtn-ice_elvtn) abs_error,
	(snow_elvtn-adj_elvtn)/snow_depth penetration,
	(adj_elvtn-ice_elvtn)/snow_depth norm_error,
	@((adj_elvtn-ice_elvtn)/snow_depth) abs_norm_error,
	(adj_elvtn>snow_elvtn)::integer above_snow,
	(adj_elvtn<ice_elvtn)::integer below_ice,
	(adj_elvtn<=snow_elvtn AND adj_elvtn >= ice_elvtn)::integer in_pack
FROM {agr_surf} JOIN _sensor USING(oid_asr)
)
"""

# COMBINE CHARACTERISTICS

combine_dist = \
"""
CREATE TABLE {output} AS (
WITH
	asr AS (
		SELECT oid_asr, roll, pitch, yaw, ocog_width
		FROM {asr_src}
	),
	wvfm AS (
		SELECT oid_asr, rwidth_full, rwidth_lead, rwidth_trail,
			pp_full, pp_left, pp_right
		FROM {asr_wvfm_char}
	),
	agr AS (
		SELECT oid_asr, dist, mgn_count, als_count, mgn_mean, mgn_range, mgn_var,
			als_mean, als_range, als_var, als_asperity_disc, als_asperity_cont,
			ise_mean, ise_range, ise_var, ise_asperity_cont, ise_asperity_disc
		FROM {agr_all}
	),
	dens AS ( SELECT oid_asr, snow_dens FROM {asr_snow_dens} )
SELECT *
FROM {error_dist} error
	JOIN asr USING (oid_asr) JOIN wvfm USING (oid_asr) JOIN dens USING (oid_asr)
	JOIN {isr_near} USING (oid_asr) JOIN agr USING(oid_asr, dist)
ORDER BY (oid_asr, dist)
)
"""

combine_footprint = \
"""
CREATE TABLE {output} AS (
WITH
	asr AS (
		SELECT oid_asr, roll, pitch, yaw, ocog_width
		FROM {asr_src}
	),
	wvfm AS (
		SELECT oid_asr, rwidth_full, rwidth_lead, rwidth_trail,
			pp_full, pp_left, pp_right
		FROM {asr_wvfm_char}
	),
	agr AS (
		SELECT oid_asr, mgn_count, als_count, mgn_mean, mgn_range, mgn_var,
			als_mean, als_range, als_var, als_asperity_disc, als_asperity_cont,
			ise_mean, ise_range, ise_var, ise_asperity_cont, ise_asperity_disc
		FROM {ftagr_all}
	),
	dens AS ( SELECT oid_asr, snow_dens FROM {asr_snow_dens} )
SELECT *
FROM {error_dist} error
	JOIN asr USING (oid_asr) JOIN wvfm USING (oid_asr) JOIN dens USING (oid_asr)
	JOIN {isr_near} USING (oid_asr) JOIN agr USING(oid_asr)
ORDER BY (oid_asr)
)
"""

combine_full = \
"""
CREATE TABLE {output} AS (
WITH
	_sensor AS (
		SELECT oid_asr, roll, pitch, yaw, ocog_width
		FROM {asr_src}
	),
	_agr AS (
	    SELECT * FROM {agr_full}
	),
	_dens AS (
	    SELECT oid_asr, snow_dens_nearest, snow_dens_avg, snow_dens_interp
	    FROM asr_snow_dens ),
	_error_spread AS (
		SELECT oid_asr, dist,
			error spread_error, abs_error spread_abs_error,
			penetration spread_penetration, norm_error spread_norm_error,
			abs_norm_error spread_abs_norm_error, above_snow spread_above_snow,
			below_ice spread_below_ice, in_pack spread_in_pack
		FROM {error_full}
		WHERE tfmra_level = {tfmra_threshold}
		    AND offset_name = {offset_name}
		    AND surf_calc = 'spread'
	),
	_error_linked AS (
		SELECT oid_asr, dist,
			error linked_error, abs_error linked_abs_error,
			penetration linked_penetration, norm_error linked_norm_error,
			abs_norm_error linked_abs_norm_error, above_snow linked_above_snow,
			below_ice linked_below_ice, in_pack linked_in_pack
		FROM {error_full}
		WHERE tfmra_level = {tfmra_threshold}
		    AND offset_name = {offset_name}
		    AND surf_calc = 'linked'
	),
	_error AS (
	    SELECT *
	    FROM _error_spread
	        JOIN _error_linked USING (oid_asr, dist)
	),
	_comb AS (
		SELECT *
		FROM _error
			FULL OUTER JOIN _agr USING (oid_asr, dist)
			FULL OUTER JOIN {asr_char} USING (oid_asr)
			FULL OUTER JOIN _sensor USING (oid_asr)
			FULL OUTER JOIN _dens USING (oid_asr)
		ORDER BY (dist, oid_asr)
	)
SELECT _comb.*
FROM _comb, {asr_optm}
WHERE _comb.oid_asr = asr_optm.oid_asr
AND _comb.dist IS NOT NULL
)
"""

select_dist_table = \
"""
SELECT * FROM {schema}.{table}
WHERE dist = {dist}
"""