"""
Functions used to perform the eureka-cryovex analysis.
Author: Paul Donchenko - University of Waterloo
"""

import numpy as np
from itertools import product, chain
from scipy import stats
from scipy import constants
import csv
from matplotlib import pyplot as plt
from matplotlib import axes as matplot_ref_axes
import pandas

import ec_config as ec
import ec_queries as eq
import pg_tools as pt

import extract_asiras_l1b
import extract_als_l1b
import extract_eureka

from collections import OrderedDict

import xlsxwriter as xlsx
import xlsxwriter.utility as xlsx_util

np.seterr(all='raise')
msg_create_spatial_index = "** creating spatial index"
msg_create_simple_index = "** creating simple index"

# SUMMARIZE MATPLOTLIB STRUCTURE
all_axes_kwargs = [name[4:] for name in dir(matplot_ref_axes.Axes)
                   if name.startswith("set_")]

# EXTRA TOOLS

def nan_mask(*args, **kwargs):
    """
    Returns a boolean array indicating non-nan indices common amongst all arrays
    in args. kwargs forwarded to numpy.isnan which checking each array.
    """
    mask = None
    for a in args:
        temp = np.logical_not(np.isnan(a,**kwargs))
        mask = np.logical_and(mask,temp) if mask is not None else temp
    return mask

def format_name(string, split_char="_", join_char=" "):
    """
    Replace split character with the join character and capitalize each word.
    """
    return join_char.join((word.capitalize() for word in
                           string.split(split_char)))

# IMPORTING SOURCE DATA

def import_asiras(conn, output, src_path, table_config=None,
                  show_debug_msg=True, col_geom=ec.col_geom,
                  src_srid=ec.src_srid, tgt_srid=ec.tgt_srid):
    """
    Import l1b level ASIRAS data from binary file to postgis database.
    """

    if table_config is None: table_config = {}
    table_config = {**table_config, 'table_name':output }

    if not pt.table_exists(conn, output):
        print("* extracting ASIRAS l1b")
        extract_asiras_l1b.extract_asiras(conn, src_path, table_config,
                                          show_debug_msg=show_debug_msg)



    if not pt.col_in_table(conn, output, col_geom):
        print("* adding geometry column for asiras")
        pt.add_geom_col(conn, output, col_geom, tgt_srid)

        print("* calculating geometry column for asiras")
        pt.calc_geom_col(conn, output, 'longitude', 'latitude', col_geom,
                      src_srid, tgt_srid)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def import_als(conn, output, src_path, table_config=None, show_debug_msg=True):
    """
    Import l1b level ALS data from binary file to postgis database.
    """

    if table_config is None: table_config = {}
    table_config = {**table_config, 'table_name':output }

    if not pt.table_exists(conn, output):
        extract_als_l1b.extract_als(conn, src_path, table_config,
                                    show_debug_msg=show_debug_msg)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def import_eureka(conn, outputs, src_path, config=None, show_debug_msg=True,
                  col_geom=ec.col_geom, src_srid=ec.src_srid,
                  tgt_srid=ec.tgt_srid):
    """
    Import ground observations from Eureka campaign to postgis database.
    """

    if not all((pt.table_exists(conn, table) for table in outputs)):
        print("* extracting eureka source")
        # Output table names coded into configuration dictionary due to
        # them being translated from the internal dataset names within the
        # h5 file
        # To modify the output names, see the extract_eureka.py script and
        # add an 'adapt_table_name' key to the config argument
        extract_eureka.extract_eureka(conn, src_path, config, True)

    for table in outputs:
        if not pt.col_in_table(conn, table, col_geom):
            print("* adding geometry column for {}".format(table))
            pt.add_geom_col(conn, table, col_geom, tgt_srid)

            print("* calculating geometry column for {}".format(table))
            pt.calc_geom_col(conn, table, 'longitude', 'latitude', col_geom,
                             src_srid, tgt_srid)

            print("* creating {} spatial index".format(table))
            pt.create_spatial_index(conn, table)

# CALCULATING OPTIMAL TRACK

# NOTE: conn is the connection object to the psql database
def create_asr_optm(conn, output, asr_src=ec.asr_src, mgn_src=ec.mgn_src,
                    max_roll_dev=ec.max_roll_dev, max_pitch_dev=ec.max_pitch_dev,
                    optm_dist=ec.optm_dist):
    """
    Create "optimal" ASIRAS table with acceptable roll,pitch and within
    a distance of snow depth observations (magnaprobe).
    """
    if not pt.table_exists(conn, output):
        print("* creating optimal ASIRAS table")
        args = {
            'identifier' : {
                'output' : output,
                'asr_src' : asr_src,
                'mgn_src' : mgn_src,
            },
            'literal' : {
                'roll_dev' : max_roll_dev,
                'pitch_dev' : max_pitch_dev,
                'dist' : optm_dist
            }
        }
        pt.execute_query(conn, eq.asr_optm, args=args)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def create_asr_optm_bufr(conn, output, asr_optm=ec.asr_optm,
                         optm_dist=ec.optm_dist):
    """
    Create buffer around optimal ASIRAS points.
    """
    if not pt.table_exists(conn, output):
        print("* creating optimal ASIRAS buffer")
        args = {
            'identifier' : {
                'output' : output,
                'asr_optm' : asr_optm,
            },
            'literal' : {
                'dist' : optm_dist
            }
        }
        pt.execute_query(conn, eq.asr_optm_bufr, args=args)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def create_asr_optm_zone(conn, output, asr_optm_bufr=ec.asr_optm_bufr):
    """
    Create single-feature polygon around optimal ASIRAS points.
    """
    if not pt.table_exists(conn, output):
        print("* creating ASIRAS zone from buffer")
        args = {
            'identifier' : {
                'output' : output,
                'asr_optm_bufr' : asr_optm_bufr
            }
        }
        pt.execute_query(conn, eq.asr_optm_zone, args=args)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def create_optm_obsv(conn, name, output, source, asr_optm=ec.asr_optm):
    """
    Filter observation points to within a proximity of optimal ASIRAS points.
    """
    if not pt.table_exists(conn, output):
        print("* creating optimal {} table".format(name))
        args = {
            'identifier' : {
                'output' : output,
                'source' : source,
                'asr_optm' : asr_optm
            },
            'literal' : {
                'dist' : ec.optm_dist
            }
        }
        pt.execute_query(conn, eq.optm_obsv, args=args)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, ec.mgn_optm)

# CREATE INTERMEDIATE DATASETS

def create_snow_dens(conn, output, asr_src=ec.asr_src,
                     snow_dens_src=ec.esc30_src):
    """
    Create reference for ASIRAS to snow density. Nearest two snow density
    measurements averaged by relative distnace for each ASIRAS nadir.
    """
    if not pt.table_exists(conn, output):
        print("* creating snow density reference")
        args = {
            'identifier' : {
                'output' : output,
                'asr_src' : asr_src,
                'esc30_src' : snow_dens_src
            }
        }
        pt.execute_query(conn, eq.snow_dens, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=1)

def create_ice_surf_elvtn(conn, output, als_src=ec.als_src, mgn_src=ec.mgn_src):
    """
    Create ice surface elevation (ISE) table by finding nearest snow surface
    elevation (ALS) point to each snow depth (magnaprobe) and subtracting the
    snow depth from the elevation.
    Geometry conforms to snow depth observations.
    """

    if not pt.table_exists(conn, output):
        print("* creating ISE (ice surface elevation) table")
        args = {
            'identifier' : {
                'output' : output,
                'als_src' : als_src,
                'mgn_src' : mgn_src
            }
        }
        pt.execute_query(conn, eq.linked_ice_elvtn, args)

        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

# CALCULATING WAVEFORM CHARACTERISTICS

def peak_threshold_index(direction, threshold):
    """
    Returns a function that finds the interpolated index of a value
    'threshold' of the maximum value in a given array, that is left/right
    (based on 'direction') and is the first to match using the 'comparison'
    function.
    """
    if direction == 'left':

        def find_left(array):
            """search towards left from start"""
            start_index = array.argmax()
            target_value = np.amax(array) * threshold

            candidates = zip(range(start_index - 1, -1, -1),
                             reversed(array[:start_index]))

            for i,v in candidates:
                if v == target_value:
                    return i
                elif v < target_value:
                    # if we've reached the left-end of the array
                    # and have found nothing so far
                    if i == 0: return None

                    x0, y0, x1, y1 = i, v, i + 1, array[i + 1]
                    return ((target_value-y0)*(x1-x0)/(y1-y0))+x0

        return find_left

    elif direction == 'right':

        def find_right(array):
            """search towards right from start"""
            start_index = array.argmax()
            target_value = np.amax(array) * threshold

            end = len(array) - 1

            candidates = zip(range(start_index + 1, len(array)),
                             array[start_index + 1:])

            for i,v in candidates:
                if v == target_value:
                    return i
                elif v < target_value:
                    # if we've reached the right-end of the array
                    # and have found nothing so far
                    if i >= end: return None

                    x0, y0, x1, y1 = i, v, i - 1, array[i - 1]
                    return ((target_value-y0)*(x1-x0)/(y1-y0))+x0

        return find_right

    else:
        raise ValueError("'direction' must be 'left' or 'right'")

def tfmra(waveform, threshold, bin_size, f_bin_elvtn):
    """
    Calculates the tfmra elevation for a waveform.
    """
    f = peak_threshold_index('left', threshold)
    return f_bin_elvtn - (np.apply_along_axis(f, 1, waveform) * bin_size)

def slice_from_peak(array, relative_index):
    """
    Returns a slice of an array relative to the index of the peak, not including
    the peak index.
    """
    # only returns the first peak
    peak_index = np.argmax(array)

    if relative_index < 0:
        sliced = array[peak_index + relative_index:peak_index]
    elif relative_index > 0:
        sliced = array[peak_index+1:peak_index + 1 + relative_index]
    else:
        sliced = array[peak_index]

    assert len(sliced) <= abs(relative_index)

    # pads the result if necessary
    if len(sliced) < abs(relative_index):
        return np.pad(sliced, [abs(relative_index) - len(sliced), 0], 'constant',
                      constant_values=np.NaN)
    elif len(sliced) == abs(relative_index):
        return sliced


def calc_wvfm_tables(conn, table_elvtn, table_char, tfmra_thresholds,
                     bin_size=ec.bin_size, signal_threshold=ec.signal_threshold,
                     c=constants.c, table_read=ec.asr_src):
    """
    Calculate characteristics of ASIRAS waveform:
        peak_elvtn - elevation of peak bin
        rwidth - width of return and before and after the peak
        pp - pulse peakiness, measure of peak value relative to mean value
    Calculate tfmra retracked elevations for given thresholds.
    """

    # skip processing if both output tables exist
    if pt.table_exists(conn, table_elvtn) and \
            pt.table_exists(conn, table_char):
        print("** output tables exist")
        return

    # Columns read and write configuration is specific to the function's
    # internal calculations and it's not helpful to refactor them outside of
    # the body of the function without making it fully dynamic.
    cols_read = ('oid_asr', 'linear_scale_factor', 'power2_scale_factor',
                 'window_delay','altitude', 'ml_power_echo')

    d_type = 'double precision'
    cols_elvtn = (
        ('oid_asr','bigint'),
        ('tfmra_level',d_type),
        ('tfmra_elvtn',d_type)
    )
    cols_char = (
        ('oid_asr','bigint'),
        ('rwidth_full', d_type),
        ('rwidth_lead', d_type),
        ('rwidth_trail', d_type),
        ('pp_full', d_type),
        ('pp_left', d_type),
        ('pp_right', d_type)
    )

    print("* calculating waveform characteristics for:\n{} to {} and {}".format(
        table_read, table_elvtn, table_char))
    print("** reading columns from database")

    # read columns from asiras table using select query
    # see 'asr_wvfm_cols' for variable descriptions
    response = pt.select(conn, table_read, cols_read,
                         as_cols=True, order_cols=cols_read[0])
    # convert response columns into numpy arrays
    arrays = ( np.array(col, dtype=float) for col in response )

    oid_asr, lsf, psf, rwc_delay, sensor_elvtn, echo, = arrays

    # all elevations in meters above WGS-84 using DGPS

    print("** precalculation")
    # get number of bins
    n_bins = echo.shape[1]

    print("** applying scaling correction")
    # -- ASIRAS Scaling Correction
    # Scale power echoes relative to each other
    # to remove effects of receive gains and attenuations
    # TODO what is the unit of the result here?
    # should be Watts according to field 26, pg 56 of data products description
    scaled = (10e-9 * (2**psf)*lsf*echo.swapaxes(0,1)).swapaxes(0,1)

    print("** calculating range window properties")
    # range window is the physical distance covered by all the bins
    rwc_size = bin_size * n_bins

    # distance from sensor to range window center (m)
    # distance from start of first bin to center of the range window
    rwc_dist = (rwc_delay * 0.5 * c)

    # delay / 2 (due to two-way travel time) times the speed of light (c)
    rwc_half = rwc_size / 2 # rwc_offset in old MATLAB code

    # distance from sensor to first bin (m)
    f_bin_dist = rwc_dist - rwc_half
    # elevation of first bin
    f_bin_elvtn = sensor_elvtn - f_bin_dist

    print("** calculating peak index and values")
    # elevation of peak returns
    peak_index = np.argmax(scaled, 1)
    peak_value = np.amax(scaled, 1)
    peak_elvtn = f_bin_elvtn - (peak_index * bin_size)

    print("** calculating return width")
    # interpolated index left of peak meeting threshold
    find_left_bound = peak_threshold_index('left', signal_threshold)
    bound_left = np.apply_along_axis(find_left_bound, 1, scaled)
    bound_left = np.ndarray.astype(bound_left, dtype=float)

    # interpolated index right of peak meeting threshold
    find_right_bound = peak_threshold_index('right', signal_threshold)
    bound_right = np.apply_along_axis(find_right_bound, 1, scaled)
    bound_right = np.ndarray.astype(bound_right, dtype=float)

    # leading edge, trailing edge and full return width
    rwidth_lead = (peak_index - bound_left) * bin_size
    rwidth_trail = (bound_right - peak_index) * bin_size
    rwidth_full = rwidth_lead + rwidth_trail

    print("** calculating pulse peakiness")
    # pulse peakiness
    pp_full = peak_value/np.sum(scaled,1)

    # pulse peakiness of three bins left of peak
    sum_left = np.nansum( np.apply_along_axis(lambda a: slice_from_peak(a, -3),
                                              1, scaled), 1)
    sum_left[sum_left==0] = np.nan
    pp_left = peak_value/sum_left

    # pulse peakiness of three bins right of peak
    sum_right = np.nansum( np.apply_along_axis(lambda a: slice_from_peak(a, 3),
                                               1, scaled), 1)
    sum_right[sum_right==0] = np.nan
    pp_right = peak_value/sum_right

    print("** calculating TFMRA retracked elevation")
    # elevation of index (interpolated) where return is certain % of peak
    # return
    tfmra_elvtn = OrderedDict()

    print("** done TFMRA thresholds:",end='')
    # calculate each threshold provided
    for threshold in tfmra_thresholds:
        elevations = tfmra(scaled, threshold, bin_size, f_bin_elvtn)
        mask = ~np.isnan(elevations)
        tfmra_elvtn[threshold] = (oid_asr[mask], elevations[mask])
        print(" {}".format(threshold), end='')
    print()

    # end with 100% of peak return (tfmra threshold of 1)
    tfmra_elvtn[1.0] = (oid_asr,peak_elvtn)

    # save output to tables

    # tfmra elevations table
    if not pt.table_exists(conn, table_elvtn):
        print("** saving results to {}".format(table_elvtn))
        pt.create_table(conn, table_elvtn, cols_elvtn)

        # sequentially combine each tfmra elevation for each row
        for level,(oid,array) in tfmra_elvtn.items():
            rows = list(zip(oid,
                            np.full(array.shape, level, dtype=float),
                            array))
            pt.insert(conn, table_elvtn, rows)


        print(msg_create_simple_index)
        pt.create_simple_index(conn, table_elvtn, use_first_cols=2)

    if not pt.table_exists(conn, table_char):
        print("** saving results to {}".format(table_char))
        pt.create_table(conn, table_char, cols_char)

        rows = zip(oid_asr, rwidth_full, rwidth_lead, rwidth_trail, pp_full,
                   pp_left, pp_right)

        pt.insert(conn, table_char, rows)
        print(msg_create_simple_index)
        pt.create_simple_index(conn, table_elvtn, use_first_cols=1)

def calc_wvfm_char(conn, read_table, write_table, schema=ec.d_schema,
                   bin_size=ec.bin_size, signal_threshold=ec.signal_threshold,
                   c=constants.c):
    """
    Calculate characteristics of ASIRAS waveform:
        tfmra 40,50,80 - threshold of first-maxima retracker elevation
        peak_elvtn - elevation of peak bin
        rwidth - width of return and before and after the peak
        pp - pulse peakiness, measure of peak value relative to mean value
    """

    if pt.table_exists(conn, write_table, schema):
        print("** output table exists")
        return

    # Columns read and write configuration is specific to the function's
    # internal calculations and it's not helpful to refactor them outside of
    # the body of the function without making it fully dynamic.
    read_cols = ('oid_asr', 'linear_scale_factor', 'power2_scale_factor',
                  'window_delay', 'instr_range_corr_ch2','altitude',
                  'ml_power_echo', 'agc_ch2')

    dct = 'double precision' # default column type
    write_cols = (
        ('oid_asr','bigserial PRIMARY KEY'),
        ('tfmra_40', dct),
        ('tfmra_50', dct),
        ('tfmra_80', dct),
        ('peak_elvtn', dct),
        ('rwidth_full', dct),
        ('rwidth_lead', dct),
        ('rwidth_trail', dct),
        ('pp_full', dct),
        ('pp_left', dct),
        ('pp_right', dct)
    )

    print("* calculating waveform characteristics for:\n{} to {}".format(
        read_table, write_table))
    print("** reading columns from database")
    # read columns from asiras table using select query
    # see 'asr_wvfm_cols' for variable descriptions
    response = pt.select(conn, read_table, read_cols,
                         as_cols=True, order_by_cols=read_cols[0])
    arrays = ( np.array(col, dtype=float) for col in response )
    oid, lsf, psf, rwc_delay, irc2, sensor_elvtn, echo, agc2 = arrays

    # all elevations in meters above WGS-84 using DGPS

    print("** precalculation")
    # get number of bins
    n_bins = echo.shape[1]

    print("** applying scaling correction")
    # -- ASIRAS Scaling Correction
    # Scale power echoes relative to each other
    # to remove effects of receive gains and attenuations
    # TODO what is the unit of the result here?
    # should be Watts according to field 26, pg 56 of data products description
    scaled = (10e-9 * (2**psf)*lsf*echo.swapaxes(0,1)).swapaxes(0,1)

    print("** calculating range window properties")
    # range window is the physical distance covered by all the bins
    rwc_size = bin_size * n_bins

    # distance from sensor to range window center (m)
    # distance from start of first bin to center of the range window
    rwc_dist = (rwc_delay * 0.5 * c)

    # alternative calculation that uses chain 2 (pg 88 in data desc. docs)
    # which corrects for range distance in LAM (low altitude mode) returns
    # likely incorrect as it moves the offset calibration further from the
    # standard
    # rwc_dist = (rwc_delay * 0.5 * c) + irc2

    # previous incorrect calculation that uses agc channel 2 as correction
    # rwc_dist = (rwc_delay * 0.5 * c) + agc2/10

    # delay / 2 (due to two-way travel time) times the speed of light (c)
    rwc_half = rwc_size / 2 # rwc_offset in old MATLAB code

    # distance from sensor to first bin (m)
    f_bin_dist = rwc_dist - rwc_half
    # elevation of first bin
    f_bin_elvtn = sensor_elvtn - f_bin_dist

    print("** calculating TFMRA retracked elevation")
    # elevation of index (interpolated) where return is certain % of peak
    # return
    tfmra40 = tfmra(scaled, 0.4, bin_size, f_bin_elvtn)
    tfmra50 = tfmra(scaled, 0.5, bin_size, f_bin_elvtn)
    tfmra80 = tfmra(scaled, 0.8, bin_size, f_bin_elvtn)

    peak_index = np.argmax(scaled, 1)
    peak_value = np.amax(scaled, 1)

    # elevation of peak returns
    peak_elvtn = f_bin_elvtn - (peak_index * bin_size)

    print("** calculating return width")
    # interpolated index left of peak meeting threshold
    find_left_bound = peak_threshold_index('left', signal_threshold)
    bound_left = np.apply_along_axis(find_left_bound, 1, scaled)
    bound_left = np.ndarray.astype(bound_left, dtype=float)

    # interpolated index right of peak meeting threshold
    find_right_bound = peak_threshold_index('right', signal_threshold)
    bound_right = np.apply_along_axis(find_right_bound, 1, scaled)
    bound_right = np.ndarray.astype(bound_right, dtype=float)

    # leading edge, trailing edge and full return width
    rwidth_lead = (peak_index - bound_left) * bin_size
    rwidth_trail = (bound_right - peak_index) * bin_size
    rwidth_full = rwidth_lead + rwidth_trail

    print("** calculating pulse peakiness")
    # pulse peakiness
    pp_full = peak_value/np.sum(scaled,1)

    # pulse peakiness of three bins left of peak
    sum_left = np.nansum( np.apply_along_axis(lambda a: slice_from_peak(a, -3),
                                              1, scaled), 1)
    sum_left[sum_left==0] = np.nan
    pp_left = peak_value/sum_left

    # pulse peakiness of three bins right of peak
    sum_right = np.nansum( np.apply_along_axis(lambda a: slice_from_peak(a, 3),
                                               1, scaled), 1)
    sum_right[sum_right==0] = np.nan
    pp_right = peak_value/sum_right

    if not pt.table_exists(conn, write_table, schema):
        print("** saving results to table")
        pt.create_table(conn, write_table, write_cols, schema)

        rows = zip(oid, tfmra40, tfmra50, tfmra80, peak_elvtn, rwidth_full,
                   rwidth_lead, rwidth_trail, pp_full, pp_left, pp_right)

        pt.insert(conn, write_table, rows, schema=schema)

        print("** creating simple index")
        pt.create_simple_index(conn, write_table, schema=schema,
                               use_first_cols=1)


# SPATIAL AGGREGATION

def create_dist_agr_isr(conn, output, round_places=ec.round_places,
                        asr_optm=ec.asr_optm, isr_optm=ec.isr_optm,
                        asr_optm_bufr=ec.asr_optm_bufr, dist_config=ec.dist_config):
    """
    Spatially aggregates RADARSAT2 ice surface roughness classification
    against ASIRAS nadir for a set of distance ranges, and obtains
    the value for the nearest point to each nadir.
    """

    if not pt.table_exists(conn, output):
        print("* aggregating ice surface roughness")

        dist_min, dist_max, dist_step = dist_config

        args = {
            'identifier' : {
                'output' : output,
                'asr_optm' : asr_optm,
                'isr_optm' : isr_optm,
                'asr_optm_bufr' : asr_optm_bufr
            },
            'literal' : {
                'dist_min' : dist_min,
                'dist_max' : dist_max,
                'dist_step' : dist_step,
                'round_places' : round_places,
            }
        }
        pt.execute_query(conn, eq.agr_isr, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)

def create_dist_agr(conn, name, prefix, output, source, value_col,
                    asr_optm=ec.asr_optm, asr_optm_bufr=ec.asr_optm_bufr,
                    asp_config=ec.asp_config, dist_config=ec.dist_config):
    """
    Spatially aggregate a set of observations against ASIRAS nadirs for a set
    of distance ranges.
    """

    if not pt.table_exists(conn, output):
        print("* spatial aggregation (dist range) of {}".format(name))
        asp_prlo, asp_prhi = asp_config
        dist_min, dist_max, dist_step = dist_config

        args = {
            'identifier' : {
                'output' : output,
                'observ' : source,
                'nadir' : asr_optm,
                'buffer' : asr_optm_bufr,
                'value' : value_col,
            },
            'literal' : {
                'pr_top' : asp_prhi,
                'pr_bottom' : asp_prlo,
                'dist_min' : dist_min,
                'dist_max' : dist_max,
                'dist_step' : dist_step
            },
            'statement' : { 'prefix' : prefix}
        }
        pt.execute_query(conn, eq.agr_dist, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)
        # NOTE: first 2 columns used since OID and dist are identifiers

def combine_agr_dist(conn, output, agr_mgn=ec.agr_mgn, agr_als=ec.agr_als,
                     agr_ise=ec.agr_ise, agr_isr=ec.agr_isr,
                     asr_optm=ec.asr_optm, dist_config=ec.dist_config):
    """
    Combine spatially aggregated tables of observations over distance ranges.
    """

    if not pt.table_exists(conn, output):
        print("* combine distance aggregated tables")
        dist_min, dist_max, dist_step = dist_config
        args = {
            'identifier' : {
                'output' : output,
                'agr_mgn' : agr_mgn,
                'agr_als' : agr_als,
                'agr_ise' : agr_ise,
                'agr_isr' : agr_isr,
                'asr_optm' : asr_optm,
            },
            'literal' : {
                'dist_min' : dist_min,
                'dist_max' : dist_max,
                'dist_step' : dist_step
            }
        }
        pt.execute_query(conn, eq.agr_combine_dist, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)

def create_optm_ftpr(conn, output, asr_optm=ec.asr_optm, asr_ftpr=ec.asr_ftpr):
    """
    Filter ASIRAS footprints to optimal records.
    """

    if not pt.table_exists(conn, output):
        print("* creating optimal footprints table")
        args = {
            'identifier' : {
                'output' : output,
                'asr_optm' : asr_optm,
                'asr_ftpr' : asr_ftpr
            }
        }
        pt.execute_query(conn, eq.create_optm_ftpr, args)
        print(msg_create_spatial_index)
        pt.create_spatial_index(conn, output)

def create_ft_agr_isr(conn, output, round_places=ec.round_places,
                      asr_optm=ec.asr_optm, isr_optm=ec.isr_optm,
                      asr_ftpr=ec.asr_ftpr):
    """
    Spatially aggregates RADARSAT2 ice surface roughness classification
    against ASIRAS nadir for a set of distance ranges, and obtains
    the value for the nearest point to each nadir.
    """

    if not pt.table_exists(conn, output):
        print("* aggregating ice surface roughness")

        args = {
            'identifier' : {
                'output' : output,
                'asr_optm' : asr_optm,
                'isr_optm' : isr_optm,
                'asr_ftpr' : asr_ftpr
            },
            'literal' : {
                'round_places' : round_places,
            }
        }
        pt.execute_query(conn, eq.ftagr_isr, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=1)

def create_ft_agr(conn, name, prefix, output, source, value_col,
                  asr_optm=ec.asr_optm, asr_ftpr=ec.asr_ftpr,
                  asp_config=ec.asp_config):
    """
    Spatially aggregate a set of observations within ASIRAS footprints.
    """

    if not pt.table_exists(conn, output):
        print("* spatial aggregation (footprint) of {}".format(name))
        asp_prlo, asp_prhi = asp_config

        args = {
            'identifier' : {
                'output' : output,
                'observ' : source,
                'asr_optm' : asr_optm,
                'asr_ftpr' : asr_ftpr,
                'value' : value_col,
            },
            'literal' : {
                'pr_top' : asp_prhi,
                'pr_bottom' : asp_prlo
            },
            'statement' : { 'prefix' : prefix}
        }
        pt.execute_query(conn, eq.agr_footprint, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=1)

def combine_ft_agr(conn, output, mgn_ftagr=ec.ftagr_mgn,
                   als_ftagr=ec.ftagr_als, ise_ftagr=ec.ftagr_ise,
                   ftagr_isr=ec.ftagr_isr, asr_optm=ec.asr_optm):
    """
    Combine spatially aggregated tables of observations within footprints.
    """

    if not pt.table_exists(conn, output):
        print("* combine footprint aggregated tables")
        args = {
            'identifier' : {
                'output' : output,
                'ftagr_mgn' : mgn_ftagr,
                'ftagr_als' : als_ftagr,
                'ftagr_ise' : ise_ftagr,
                'ftagr_isr' : ftagr_isr,
                'asr_optm' : asr_optm,
            }
        }
        pt.execute_query(conn, eq.agr_combine_footprint, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=1)

def combine_agr_full(conn, output, ftagr_all=ec.ftagr_all,
                     agr_all=ec.agr_all, ft_dist=ec.ft_dist):
    """
    Combine footprint and distance aggregated tables by giving footprint
    a specific distance rank.
    """
    if not pt.table_exists(conn, output):
        print("* combining all aggregated tables")
        args = {
            'identifier' : {
                'output' : output,
                'ftagr_all' : ftagr_all,
                'agr_all' : agr_all
            },
            'literal' : {
                'ft_dist' : ft_dist
            }
        }
        pt.execute_query(conn, eq.agr_combine_full, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)

def calc_agr_surface(conn, output, agr_full=ec.agr_full):
    """
    Calculate ice surface elevation using both spread and linked aggregations
    """
    if not pt.table_exists(conn, output):
        print("* calculating observed ice surface elevations")
        args = {
            'identifier' : {
                'output' : output,
                'agr_full' : agr_full
            }
        }
        pt.execute_query(conn, eq.agr_surface, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)


# ANALYZING CALIBRATION

def csv_to_dict(file_path, key_field, skip_lines=0, newline='\n',
                apply_to_keys=None, apply_to_header=None):
    """
    Read csv file into dict based on key_field. Input file must have a header.
    """
    with open(file_path, newline=newline) as f:
        reader = csv.reader(f)

        for i in range(skip_lines): next(reader)

        header = next(reader)

        if apply_to_header is not None:
            header = [ apply_to_header(field) for field in header ]

        key_index = header.index(key_field)
        header.pop(key_index)

        data = {}

        for row in reader:
            key = row.pop(key_index)
            if apply_to_keys:
                key = apply_to_keys(key)
            data[key] = { header[i]:val for i,val in enumerate(row) }

    return data

def calib_shallow_snow(conn, targets_path, tfmra_threshold,
                       ftagr_all=ec.ftagr_all, asr_elvtn=ec.asr_elvtn,
                       quality_colors=ec.quality_colors,
                       quality_means=ec.quality_means):
    """
    Calibrate offset between ALS and ASIRAS altimetry based on shallow snow
    elevations.

    Uses spread estimate of observed ice elevation due to higher accuracy.
    TODO - is it actually higher accuracy?
    """

    # get returns that were identified as having shallow snow
    # and in a flat-ice surface zone
    targets = csv_to_dict(targets_path, 'oid_asr', apply_to_keys=int)

    # get the estimated elevation and aggregated snowpack characteristics
    # for each return that was in targets
    query = \
    """
    SELECT oid_asr, als_mean, mgn_mean, tfmra_elvtn
    FROM {ftagr_all} JOIN {asr_elvtn} USING (oid_asr)
    WHERE tfmra_level = {tfmra_threshold} AND oid_asr IN ({oid_list})
    """
    args = {
        'identifier':{
            'ftagr_all':ftagr_all, 'asr_elvtn':asr_elvtn
        },
        'literal' : {
            'oid_list':[ int(i) for i in targets.keys() ],
            'tfmra_threshold':tfmra_threshold}
    }
    response = pt.execute_query(conn, query, args, fetchall=True)
    for oid_asr, als_mean, mgn_mean, elvtn_est in response:
        target = targets[oid_asr]
        target['snow_depth'] = mgn_mean
        target['offset'] = (als_mean-mgn_mean) - elvtn_est

    quality_list = list(quality_colors.keys())

    # sort target returns and their aggregated snowpack characteristics
    # by quality
    by_quality = { q:[ (v['snow_depth'],v['offset']) for k,v in
                          targets.items() if v['quality']==q ]
                   for q in quality_list }

    # summarize the mean offset for each group of quality levels
    mean_offsets = {}
    for group in quality_means:
        offsets = []
        for quality in group:
            offsets.extend([ row[1] for row in by_quality[quality] ])
        mean_offsets[group] = sum(offsets)/len(offsets)


    # graph the results
    plt.figure()
    quality_legend = []
    for quality,rows in by_quality.items():
        x,y = zip(*rows)
        plt.scatter(x,y,c=quality_colors[quality])
        # ensure the legend is in the same order as the qualities
        quality_legend.append(quality)
    plt.legend(quality_legend)
    plt.title("Shallow snow calibration offsets")

    # prettify the key for the mean offsets before returning
    shallow_offsets = { '_'.join(k):v for k,v in mean_offsets.items() }
    return shallow_offsets

def analyze_offset_list(conn, offsets, tfmra_threshold,
                        ftagr_all=ec.ftagr_all, asr_elvtn=ec.asr_elvtn):
    """
    Compare a selection of offsets based on statistics of their error.
    """

    offset_pairs = [ (k,v) for k,v in offsets.items() ]

    args = {
        'identifier':{'ftagr_all':ftagr_all, 'asr_elvtn':asr_elvtn},
        'literal' : {'tfmra_threshold':tfmra_threshold}
    }
    parsed_args = {
        # convert offset name,value pairs to parsed SQL statement
        # both name and value are literals (L)
        'values_list' : pt.parse_values_list(offset_pairs, ('L','L'))
    }

    descr, response = pt.execute_query(conn, eq.analyze_offsets,
                                       args, parsed_args, fetchall=True,
                                       description=True)
    col_names = [col.name for col in descr ]

    return pandas.DataFrame(response, None, col_names)

def analyze_offset_range(conn, offset_min, offset_max, offset_step,
                         tfmra_threshold, ftagr_all=ec.ftagr_all,
                         asr_elvtn=ec.asr_elvtn):
    """
    Compare a range of offsets based on the statistics of their error.
    Uses only RMSE since other stats are compared in analyse_offsets
    """

    args = {
        'identifier':{'ftagr_all':ftagr_all, 'asr_elvtn':asr_elvtn},
        'literal':{'offset_min':offset_min, 'offset_max':offset_max,
                   'offset_step':offset_step,
                   'tfmra_threshold':tfmra_threshold}
    }
    response = pt.execute_query(conn, eq.analyze_offset_range, args,
                                fetchall=True, as_cols=True)

    offset_val, rmse_spread, rmse_linked = response

    plt.figure()
    plt.plot(offset_val, rmse_spread)
    plt.plot(offset_val, rmse_linked)
    plt.legend(['RMSE error spread','RMSE error linked'])
    plt.title("RMSE values for ALS-ASIRAS offsets")

    # Find the optimal offset
    optimal_offsets = {
        'optimal_rmse_spread':
                float(offset_val[rmse_spread.index(min(rmse_spread))]),
        'optimal_rmse_linked':
                float(offset_val[rmse_linked.index(min(rmse_linked))])
    }

    return optimal_offsets

# CALCULATING ERROR

def calc_error_full(conn, output, offset_pairs, asr_elvtn=ec.asr_elvtn,
                    agr_surf=ec.agr_surf):
    """
    Calculate measurements of error for all combinations of tfmra retracked
    elevation and surface observation.

    Characteristics of observation combination:
        dist - aggregation distance range from nadir (also includes footprint)
        surf_calc - surface calculation type, spread or linked
        tfmra_level - TFMRA retracked % threshold
        offset_name/val - ALS-ASIRAS offset distance and source of value

    Measures of error:
        error, abs_error - distance between observed and estimated ice surface
            elevation
        penetration - how far (0 to 1) estimated ice surface elevation is
            into snowpack from above
        norm_error, abs_norm_error - error normalized to snow depth
        above_snow, below_ice, in_pack - location of observed elevation
            relative to observed snow pack boundaries
    """

    if not pt.table_exists(conn, output):
        print("* calculating full characteristic combination error (~5 min)")
        args = {
            'identifier' : {
                'output' : output,
                'asr_elvtn' : asr_elvtn,
                'agr_surf' : agr_surf
            }
        }
        parsed_args = {
            'values_list' : pt.parse_values_list(offset_pairs, ('L','L'))
        }
        pt.execute_query(conn, eq.error_full, args, parsed_args)

        print(msg_create_simple_index)
        # index on all characteristics, of which there are five + oid
        pt.create_simple_index(conn, output, use_first_cols=6)


# COMBINING CHARACTERISTICS

def combine_char_dist(conn, output, asr_src=ec.asr_src,
                      asr_wvfm_char=ec.asr_wvfm_char, agr_all=ec.agr_all,
                      error_dist=ec.error_dist, isr_near=ec.isr_near,
                      asr_snow_dens=ec.asr_snow_dens):
    """
    Combine tables showing characteristics of return signal, error, and surface
    observations aggregated over distance ranges.
    """

    if not pt.table_exists(conn, output):
        print("* combining features using distance aggregation")
        args = {
            'identifier' : {
                'output' : output,
                'asr_src' : asr_src,
                'asr_wvfm_char' : asr_wvfm_char,
                'agr_all' : agr_all,
                'error_dist' : error_dist,
                'isr_near' : isr_near,
                'asr_snow_dens' : asr_snow_dens
            }
        }
        pt.execute_query(conn, eq.combine_dist, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)

def combine_char_footprint(conn, output, asr_src=ec.asr_src,
                           asr_wvfm_char=ec.asr_wvfm_char,
                           ftagr_all=ec.ftagr_all, error_dist=ec.error_dist,
                           isr_near=ec.isr_near,
                           asr_snow_dens=ec.asr_snow_dens):
    """
    Combine tables showing characteristics of return signal, error, and surface
    observations aggregated over footprints.
    """

    if not pt.table_exists(conn, output):
        print("* combining features using footprint aggregation")
        args = {
            'identifier' : {
                'output' : output,
                'asr_src' : asr_src,
                'asr_wvfm_char' : asr_wvfm_char,
                'ftagr_all' : ftagr_all,
                'error_dist' : error_dist,
                'isr_near' : isr_near,
                'asr_snow_dens' : asr_snow_dens
            }
        }
        pt.execute_query(conn, eq.combine_footprint, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=1)

def combine_char(conn, output,
                 tfmra_threshold, surf_calc_type, offset_name,
                 asr_src=ec.asr_src, asr_char=ec.asr_char,
                 agr_full=ec.agr_full, error_full=ec.error_full,
                 asr_optm=ec.asr_optm, asr_snow_dens=ec.asr_snow_dens):

    if not pt.table_exists(conn, output):
        print("* combining all characteristics")
        args = {
            'identifier' : {
                'output' : output,
                'asr_src' : asr_src,
                'asr_optm' : asr_optm,
                'asr_char' : asr_char,
                'agr_full' : agr_full,
                'error_full' : error_full,
                'asr_snow_dens' : asr_snow_dens
            },
            'literal' : {
                'tfmra_threshold' : tfmra_threshold,
                'surf_calc_type' : surf_calc_type,
                'offset_name' : offset_name
            }
        }

        pt.execute_query(conn, eq.combine_full, args)

        print(msg_create_simple_index)
        pt.create_simple_index(conn, output, use_first_cols=2)


# NUMPY AND SCIPY EXTRAS

def check_arrays_normal(cols, normal_thresh=ec.normal_thresh,
                        unique_thresh=ec.unique_thresh):
    """
    Returns a boolean array indicating which columns in 'cols' are normally
    distributed.
    """
    num_cols = cols.shape[0]

    # boolean array to store whether each column is normal
    normal_mask = np.zeros((num_cols,), dtype='bool')

    for i in range(num_cols):
        # use only non-nan values
        target = cols[i, ~np.isnan(cols[i, :])]

        # check if column has enough unique elements
        if np.unique(target).shape[0] > unique_thresh:
            s,p = stats.normaltest(target)

            # check significance of null hypothesis (that distr. is normal)
            # reject the null-hyp. (low p) - non-normal
            # don't reject (high p) - normal
            normal_mask[i] = p > normal_thresh

        else:
            normal_mask[i] = False

    return normal_mask

# ANALYSIS FUNCTIONS

def cross_corr_array(block, corr_func):
    """
    Return the correlation statistic and p-value among a set of arrays
    in 'block' using 'corr_function'.
    """

    num_cols = block.shape[0]

    s_vals = np.full((num_cols, num_cols), np.nan) # statistic
    p_vals = np.full((num_cols, num_cols), np.nan) # significance (p-value)

    # iterate through each combination of columns
    for i1, i2 in product(range(num_cols), repeat=2):

        # rows where both columns i1 and i2 in cols are not nan
        mask = nan_mask(block[i1,:], block[i2,:])

        s, p = corr_func(block[i1,mask], block[i2,mask])

        s_vals[i1,i2] = s
        p_vals[i1,i2] = p

    return s_vals, p_vals

def cross_corr_table_to_xlsx(conn, excel_path, table, dist_level,
                             schema=ec.d_schema,
                             exclude_cols=('oid_asr','dist'),
                             reformat_col_names=True,
                             col_names_split='_',
                             significance=ec.significance,
                             freeze_panes=True,
                             mark_selfcorr=True):
    """
    Calculates cross-correlation for a table of characteristics and writes
    the output for an excel table.
    """

    # get the table from the serve
    args = {
        'literal' : { 'dist_level' : dist_level }
    }
    where_clause = "dist = {dist_level}"
    descr, response = pt.select(conn, table,
                                where_clause=where_clause, args=args,
                                description=True, as_cols=True, schema=schema,
                                exclude_cols=exclude_cols)

    # extract the column names
    raw_col_names = [ col.name for col in descr ]

    # reformat the names if required
    col_names = [format_name(name) for name in raw_col_names] \
        if reformat_col_names else raw_col_names

    # TODO takes too much time to correctly apply borders
    # # attempt to break the column names into groups to draw borders in the table
    # if draw_category_borders:
    #     break_indices = []
    #
    #     # break each column name and take the first word only
    #     first_words = ( name.split(col_names_split)[0]
    #                     for name in raw_col_names )
    #     streak = 0
    #     last_word = None
    #
    #     for index,word in enumerate(first_words):
    #         if word == last_word:
    #             if streak > 1:
    #                 break_indices.append(index-1)
    #                 streak = 0
    #         else:
    #             if streak == 0:
    #                 break_indices.append(index)
    #             streak += 1
    #         last_word = word

    # convert the response columns to a numpy array
    col_data = np.array(response, dtype=float)

    # iteration configuration for each statistic to check
    iconfig = (
        ('spearman_r','spearman_p', stats.spearmanr),
        ('kendall_tau','kendall_p', stats.kendalltau)
    )

    # open excel workbook
    workbook = xlsx.Workbook(excel_path, {'nan_inf_to_errors': True})

    # locally define header row and index column offsets
    # use only one row and column for headers
    ofs_r = 1
    ofs_c = 1

    # define conditional formatting formats
    format_scaled = {
        'type' : '3_color_scale',
        'min_color' : "#6c96ff",
        'mid_color' : "#ffffff",
        'max_color' : "#ff6c6c"
    }
    # decimal places for numbers
    format_number = workbook.add_format({
        'num_format' : "0.00"
    })
    # top and left-side headers
    format_header = workbook.add_format({
        'bold':True, 'text_wrap':True
    })
    # non-significant r-values
    format_insig = workbook.add_format({
        'bg_color' : '#8c8c8c',
        'font_color' : '#ffffff'
    })
    # self correlation blackout
    format_blackout = workbook.add_format({
        'bg_color' : '#000000',
        'font_color' : '#dddddd'
    })
    format_selfcorr = {
        'type':'cell',
        'criteria':'=',
        'value':1,
        'stop_if_true':True,
        'format':format_blackout,
    }
    # iterate through
    for sheet_name_s, sheet_name_p, stat_func in iconfig:

        # red-blue coloring based on r value
        # defined differently for each pair of sheets
        format_pval = {
            'type' : 'formula',
            'criteria': "='{}'!{}>{}".format(
                sheet_name_p,
                xlsx_util.xl_rowcol_to_cell(ofs_c,ofs_r),
                significance
            ),
            'stop_if_true' : True,
            'format' : format_insig
        }


        # calculate statistic and significance (p value)
        result_s, result_p = cross_corr_array(col_data,stat_func)
        # height and width should be the same since the matrix is square
        size = result_s.shape[0]

        sheet_s = workbook.add_worksheet(sheet_name_s)
        sheet_p = workbook.add_worksheet(sheet_name_p)

        # write headers
        for i in range(size):
            sheet_s.write_string(0,i+1,col_names[i], format_header)
            sheet_s.write_string(i+1,0,col_names[i], format_header)
            sheet_p.write_string(0,i+1,col_names[i], format_header)
            sheet_p.write_string(i+1,0,col_names[i], format_header)

        # write values
        for x,y in product(range(size),repeat=2):
            sheet_s.write_number(y+ofs_r, x+ofs_c, result_s[x,y],
                                 format_number)
            sheet_p.write_number(y+ofs_r, x+ofs_c, result_p[x,y],
                                 format_number)

        # apply self-correlation blackout
        for i in range(1,size+1):
            sheet_s.conditional_format(i,i,i,i,format_selfcorr)

        # apply conditional formatting for visualization
        sheet_s.conditional_format(ofs_r,ofs_r,ofs_r+size,ofs_c+size,
                                   format_pval)
        sheet_s.conditional_format(ofs_r,ofs_r,ofs_r+size,ofs_c+size,
                                   format_scaled)

        cell_format = workbook.add_format({'bold': True})
        sheet_s.set_column(5,5,None,cell_format)

        #

        # freeze panes if necessary
        if freeze_panes:
            sheet_s.freeze_panes(ofs_r,ofs_c)
            sheet_p.freeze_panes(ofs_r,ofs_c)

    workbook.close()

def result_plot(result_dict, axes, legend=True, legend_drag=True,
                legend_title=True, legend_kwargs=None, format_names=True,
                **axes_kwargs):
    """
    Plots the results of a table extraction to axes based on formatting
    properties.
    """

    axes.plot(*chain(*result_dict['data']))

    proc_name = lambda name: format_name(name) if format_names else name

    # process legend keyword arguments
    if legend_kwargs is None: legend_kwargs = {}
    if legend_title is True and 'key_name' in result_dict \
        and 'title' not in legend_kwargs:
        legend_kwargs['title'] = proc_name(result_dict['key_name'])
    elif isinstance(legend_title,str):
        legend_kwargs['title'] = legend_title

    # process y-axis variable names if they exist
    y_names = [ proc_name(name) for name in result_dict.get('y_names',[]) ]

    # use default legend or one that is provided in the argument if it fits
    if legend is True:
        legend_ref = axes.legend(y_names, **legend_kwargs)
    elif pt._is_non_string_sequence(legend):
        legend_ref = axes.legend(legend)

    if legend_drag: legend_ref.draggable()


    # apply relevant keyword arguments to axes
    axes.set(**{k:v for k,v in axes_kwargs.items() if k in all_axes_kwargs})




