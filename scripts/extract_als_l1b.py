"""
Reads ALS (airborne laser sensor) l1b file, and imports to postgresql database
with postgis geometry points.
Author: Paul Donchenko - University of Waterloo
"""

from pg_tools import table_exists, drop_table, create_table, insert, \
    add_geom_col

from struct import unpack
from math import isnan
from sys import stdout

# L1B HARD SETTINGS
file_read_format = 'rb'
debug_buffer_msg = "Processed {}/{} lines ( {:.2f}% )"
geom_type = "POINT"
geom_dim = 2

# DEFAULT SETTINGS
# writing to pg table
d_table_config = {
    'overwrite_table' : False,
    'schema_name' : 'public',
    'table_name' : 'als',
    'id_name' : 'oid_als',
    'id_type' : 'bigserial PRIMARY KEY',
    'value_name' : 'snow_elvtn',
    'value_type' : 'double precision',
    'geom_name' : 'geom',
    'src_srid' : 4326,
    'tgt_srid' : 5921,
    'insert_template' :\
        "(%s,ST_Transform(ST_SetSRID("
        "ST_MakePoint(%s, %s), {src_srid}),{tgt_srid}))"
}
# reading l1b als file soft settings
d_read_config = {
    'header_size' : 36,
    'header_format' : ">BLBHQHBBLL8B",
    'header_stat_indices' : (1,3),
    'theader_line_size' : 4,
    'var_size' : 8,
    'var_format' : ">{}d",
    'bufferCount' : 100 # number of buffer sections
}

def extract_als(connection, source_path, table_config=None, read_config=None,
                show_debug_msg=False):
    """
    Extract ALS l1b data from binary file to postgis enabled psql database.
    """

    # add items to dict from default dict if the user didn't include them
    read_config = d_read_config if not read_config else\
        {**d_read_config, **read_config}
    table_config = d_table_config if not table_config else\
        {**d_table_config, **table_config}

    with open(source_path, file_read_format) as source:

        # - Build configuration from source file header
        if show_debug_msg:
            stdout.write("Reading source file at {}\n".format(source_path))
            stdout.flush()

        # read header from source file
        header = unpack(
            read_config['header_format'],
            source.read(read_config['header_size'])
        )

        # get file stats from header
        start,end = read_config['header_stat_indices']
        num_lines, points_per_line = header[start:end]

        # bytes used by each variable for a single line
        line_var_size = read_config['var_size'] * points_per_line
        # format used to read each variable
        line_var_format = read_config['var_format'].format(points_per_line)

        # skip timestamp header
        source.read(read_config['theader_line_size'] * num_lines)

        # - Setup target table
        if show_debug_msg:
            stdout.write("Setting up target database table\n")
            stdout.flush()

        table_name = table_config['table_name']
        schema_name = table_config['schema_name']

        # drop table if it exists and can be dropped
        if table_exists(connection, table_name):
            if table_config['overwrite_table']:
                drop_table(connection, table_name)

                if show_debug_msg:
                    stdout.write("Dropped table {}.{}\n".format(
                        schema_name, table_name))
                    stdout.flush()

            else:
                raise Exception("Table {}.{} exists but cannot be overwritten "
                                "due to settings".format(
                    schema_name,table_name))

        # setup column formatting
        create_col_names = [table_config[key] for key in
                            ('id_name', 'value_name')]
        create_col_types = [table_config[key] for key in
                            ('id_type', 'value_type')]
        cols_config = list(zip(create_col_names, create_col_types))

        insert_col_names = [table_config[key] for key in
                            ('value_name', 'geom_name')]

        # setup insert query template
        src_srid = table_config['src_srid']
        tgt_srid = table_config['tgt_srid']
        template = table_config['insert_template']
        template_args = {
            'literal':{'src_srid' : src_srid, 'tgt_srid' : tgt_srid }
        }

        # create table
        create_table(connection, table_name, cols_config, schema_name)

        if show_debug_msg:
            stdout.write("Created table {}.{}\n".format(
                table_name,schema_name))
            stdout.flush()

        # add geometry column with target SRID
        add_geom_col(connection, table_name, table_config['geom_name'],
                     tgt_srid, geom_type, geom_dim, schema_name)

        # - Read source file and write points to database
        if show_debug_msg:
            stdout.write("Streaming file to database\n")
            stdout.flush()

        # initialize buffer and counters
        read_lines = 0
        written_points = 0
        block_lines = 0
        buffer_lines = num_lines / read_config['bufferCount']
        out_chars_max = 0

        # hold rows ready to write to database
        buffer = []

        while read_lines < num_lines:

            # read a single line (each variable 'points_per_line' times)
            # 'time' is not used currently
            line_time = unpack(line_var_format,source.read(line_var_size))
            line_lat = unpack(line_var_format,source.read(line_var_size))
            line_lon = unpack(line_var_format,source.read(line_var_size))
            line_elv = unpack(line_var_format,source.read(line_var_size))

            # package variables into per-point tuples
            for elv,lon,lat in zip(line_elv,line_lon,line_lat):
                # filter out rows with any NaN values
                if not (isnan(elv) or isnan(lon) or isnan(lat)):
                    buffer.append((elv,lon,lat))
                    written_points += 1

            block_lines += 1
            read_lines += 1

            # write buffer cols if enough lines have been read or end of file
            if block_lines >= buffer_lines or read_lines == num_lines:

                insert(connection, table_name, buffer, insert_col_names,
                       template, template_args, schema_name)

                # reset buffer and counter
                buffer = []
                block_lines = 0

                # update stdout debug message showing read progress
                if show_debug_msg:
                    msg = debug_buffer_msg.format(read_lines, num_lines,
                                                  100 * read_lines / num_lines)
                    if len(msg) > out_chars_max: out_chars_max = len(msg)
                    stdout.write('\r' + msg + ' '*max(0,out_chars_max-len(msg)))
                    stdout.flush()

        if show_debug_msg:
            stdout.write("\nFinished reading {} lines, wrote {} points\n".\
                format( read_lines, written_points))
            stdout.flush()

    return