"""
Reads ASIRAS (Assessment of SAR/Interferometric radar altimeter concept) l1b
file, applies unit scaling and imports to postgresql database.
Author: Paul Donchenko - University of Waterloo
"""

import re
from itertools import chain
from struct import unpack, calcsize
from pg_tools import table_exists, drop_table, create_table, insert
from sys import stdout

# L1B HARD SETTINGS
d_parse_tokens = ('default', 'l', '1', '1', 'double precision')
file_read_format = 'rb'
debug_buffer_msg = "Processed {}/{} blocks ( {:.2f}% )"

# DEFAULT SETTINGS
d_table_config = {
    'overwrite_table' : False,
    'schema_name' : 'public',
    'table_name' : 'asiras',
    'id_name' : 'oid_asr',
    'id_type' : 'bigserial PRIMARY KEY'
}

d_read_config = {
    'rows_per_block' : 20,
    'header_encoding' : 'ASCII',
    'header_pattern' : r"""([a-z_]+)="?([^\n<>"]+)(?:<[^\n<>]+>)?"?\n""",
    'mph_bytes' : 1247,
    'sph_bytes' : 1112,
    'dsd_bytes' : 280,
    'asr_dsd_prefix' : 'ASI',
    'byte_order' : '>',
    'buffer_count' : 100, # number of times to buffer

    # Variable group reading sizes - for skipping empty groups
    # Correction Group
    'size_cg' : 64,
    # Average Pulse-width Limited Waveform Group
    'size_awg' : 556,

    # Variable group reading formats:
    # (name, read type, scale, count, write type)
    'format_tog' : \
"""
days,l,,,bigint
seconds,L,,,bigint
miroseconds,L,,,bigint
~,x,,8
instrument_config,L,,,bigint
burst_counter,L,,,bigint
latitude,l,1e-7
longitude,l,1e-7
altitude,l,1e-3
altitude_rate,l,1e-6
velocity_xyz,l,1e-3,3,double precision[3]
beam_direction_xyz,l,1e-6,3,double precision[3]
interferometer_baseline_xyz,l,1e-6,3,double precision[3]
confidence_data,L,,,bigint
""",

    'format_mg' : \
"""
window_delay,q,1e-12
~,x,,4
ocog_width,l,1/100
retracker_range,l,1e-3
surface_elvtn,l,1e-3
agc_ch1,l,1/100
agc_ch2,l,1/100
tfg_ch1,l,1/100
tfg_ch2,l,1/100
transmit_power,l,1e-6
doppler_range_corr,l,1e-3
instr_range_corr_ch1,l,1e-3
instr_range_corr_ch2,l,1e-3
~,x,,8
intern_phase_corr,l,1e-6
extern_phase_corr,l,1e-6
noise_power,l,1/100
roll,h,1e-3
pitch,h,1e-3
yaw,h,1e-3
~,x,,2
heading,l,1e-3
std_roll,H,1e-4
std_pitch,H,1e-4
std_yaw,H,1e-4
""",

    'format_mwg': \
"""
ml_power_echo,H,,256,bigint[256]
linear_scale_factor,l,,,bigint
power2_scale_factor,l,,,bigint
num_ml_echoes,H,,,bigint
flags,H,,,integer
beam_behaviour,H,,50,bigint[50]
"""
}


def extract_asiras(connection, source_path, table_config=None, read_config=None,
                   show_debug_msg=False):
    """
    Extract ASIRAS l1b data from binary file to postgis enabled psql database.
    """

    def _parse_header(size):
        """Extract key:value pairs from l1b header text"""
        header_string = source.read(size).decode(read_config['header_encoding'])
        pairs_iterator = re.finditer(
                read_config['header_pattern'],header_string,re.I)

        return { pair[1]:pair[2].strip() for pair in pairs_iterator }

    def _parse_group_format_text(text, line_split='\n', delimiter=',',
                                 spare='~', no_value=''):

        # configuration for variable sequence
        sq_name = []
        sq_r_format = []
        sq_r_size = []
        sq_has_value = []
        sq_scale = []
        sq_is_list = []
        sq_w_type = []

        for line in text.strip().split(line_split):

            tokens = line.strip().split(delimiter)

            if len(tokens) > len(d_parse_tokens):
                raise ValueError("Too many tokens for line: {}".format(line))

            # replace missing token items with default if missing
            tokens = [d if i >= (len(tokens)) or tokens[i] == no_value
                       else tokens[i]
                      for i,d in enumerate(d_parse_tokens)]

            # build configuration for line based on tokens
            name,r_type,scale,count,w_type = tokens

            # name
            sq_name.append(name)

            # read format and count
            try:
                if int(count) > 1:
                    is_list = True
                else:
                    count = ""
                    is_list = False
            except ValueError:
                raise ValueError("Count must be int for line: {}".format(line))

            read_format = read_config['byte_order'] + count + r_type
            sq_r_format.append(read_format)

            # size to read from file
            sq_r_size.append(calcsize(read_format))

            # whether to read the unpacked value (spare or not)
            sq_has_value.append(name != spare)

            # scale
            eval_scale = eval(scale)
            if isinstance(eval_scale, int) or isinstance(eval_scale, float):
                sq_scale.append(eval_scale)
            else:
                raise ValueError("Scale must be int or float for line: {}".\
                                 format(line))

            # whether multiple values will be unpacked
            sq_is_list.append(is_list)

            # database writing type
            sq_w_type.append(w_type)

        # zipped into tuples which contains the formatting parameters:
        # name, read format, read size, has value, is list, scale, write type
        group_format = list(zip(sq_name, sq_r_format, sq_r_size, sq_has_value,
                                sq_is_list, sq_scale, sq_w_type))

        return group_format

    def _get_group_read_format(group_format):
        """Get the read parameters from the group format using local config."""

        # extract only these parameters:
        # rFormat, rSize, hasValue, is_list, scale
        return [ varFormat[1:6] for varFormat in group_format]

    def _get_create_table_format(group_formats):
        """Get name,writetype pairs from format groups to create table."""

        oid_pair = [(d_table_config['id_name'], d_table_config['id_type'])]

        # extract only these parameters:
        # name, write type
        # if hasValue is True
        return oid_pair + [ (varFormat[0],varFormat[6])
                           for varFormat in chain(*group_formats)
                           if varFormat[3] ]

    def _get_insert_format(group_formats):
        """Get name parameters from format. Used as column names for insert
        query."""
        # extract only name parameters if hasValue is True
        return [ varFormat[0] for varFormat in chain(*group_formats)
                 if varFormat[3] ]

    def _read_item_group(group_read_format):
        """Read a group of variables from source file based on format spec."""
        var_data = []

        for rFormat,rSize,hasValue,isList,scale in group_read_format:
            data_string = source.read(rSize)
            if hasValue:
                result = unpack(rFormat,data_string)
                if isList:
                    var_data.append([ v*scale for v in result ])
                else:
                    var_data.append(result[0]*scale)

        return var_data

    # add items to dict from default dict if the user didn't include them
    read_config = d_read_config if not read_config else\
        {**d_read_config, **read_config}
    table_config = d_table_config if not table_config else\
        {**d_table_config, **table_config}

    with open(source_path, file_read_format) as source:

        # - Setup for reading based on file headers
        if show_debug_msg:
            stdout.write("Reading source file at {}\n".format(source_path))
            stdout.flush()

        # main product header (MPH)
        mph = _parse_header(read_config['mph_bytes'])
        # specific product header (SPH) - not used
        sph = _parse_header(read_config['sph_bytes'])

        # determine number of datasets in header
        dsd_count = int(mph['NUM_DSD'])

        # read dataset headers (DSD)
        dsd = [_parse_header(read_config['dsd_bytes']) for i in
               range(dsd_count)]

        # ASIRAS should be the first dataset, raise exception if it's not
        if not dsd[0]['DS_NAME'].startswith(read_config['asr_dsd_prefix']):
            raise Exception("First dataset header is an ASIRAS dataset")

        # find seek location (bytes from start of file) for ASIRAS dataset
        asiras_file_start = int(dsd[0]['DS_OFFSET'])
        # seek to that location
        source.seek(asiras_file_start)

        # - Setup l1b group formats
        # this reads the text cols definining groups of variables and converts
        # it into a list of tuple structs that are then used to build format
        # lists for reading the l1b file, creating a db table and writing
        # values to the table
        format_tog = _parse_group_format_text(read_config['format_tog'])
        format_mg = _parse_group_format_text(read_config['format_mg'])
        format_mwg = _parse_group_format_text(read_config['format_mwg'])

        # - Setup l1b reading format\
        # build format structs to read l1b variable groups
        read_format_tog = _get_group_read_format(format_tog)
        read_format_mg = _get_group_read_format(format_mg)
        read_format_mwg = _get_group_read_format(format_mwg)

        # get bytes to skip for empty groups
        size_cg = read_config['size_cg']
        size_awg= read_config['size_awg']

        # get number of blocks (same as records)
        # each record has 20 rows with the fields grouped into blocks
        # e.g.
        #
        # BLOCK 1
        #   TOG:    row 1,2,3,..,20
        #   MG:     row 1,2,3,..,20
        #   MWG:    row 1,2,3,...,20
        # BLOCK 2
        #   ...
        num_blocks = int(dsd[0]['NUM_DSR'])

        # number of times each group has to be read in sequence before moving
        # onto next cols
        rows_per_block = read_config['rows_per_block']

        # - Setup target table
        if show_debug_msg:
            stdout.write("Setting up target database table\n")
            stdout.flush()

        table_name = table_config['table_name']
        schema_name = table_config['schema_name']

        # drop table if it exists and can be dropped
        if table_exists(connection, table_name):
            if show_debug_msg:
                    stdout.write("Exists table: {}.{}\n".format(
                        schema_name, table_name))
                    stdout.flush()

            if table_config['overwrite_table']:
                drop_table(connection, table_name)

                if show_debug_msg:
                    stdout.write("Dropped table: {}.{}\n".format(
                        schema_name, table_name))
                    stdout.flush()

            else:
                raise Exception("Table {}.{} exists but cannot be overwritten "
                                "due to settings".format(schema_name,table_name))

        else:
            if show_debug_msg:
                stdout.write("No such table: {}.{}\n".format(
                    schema_name, table_name))
                stdout.flush()

        # NOTE: order of groups must be same as in l1b file
        create_cols_config = _get_create_table_format(
            (format_tog, format_mg, format_mwg))

        # create table in the connected Pg database
        create_table(connection, table_name, create_cols_config, schema_name)

        if show_debug_msg:
            stdout.write("Created table {}.{}\n".format(
                table_name,schema_name))
            stdout.flush()

        # list of column names used to form insert rows query
        insert_col_names = _get_insert_format(
            (format_tog, format_mg, format_mwg))

        # number of blocks to hold in memory before writing to database
        buffer_blocks = num_blocks / read_config['buffer_count']

        # - Read source file and write points to database
        if show_debug_msg:
            stdout.write("Streaming file to database\n")
            stdout.flush()

        # initialize buffers and
        written_rows = 0
        read_blocks = 0
        buffered_blocks = 0
        out_chars_max = 0

        # hold rows ready to write to database
        buffer = []

        while read_blocks < num_blocks:

            tog = [ _read_item_group(read_format_tog) for
                    i in range(rows_per_block) ]
            mg = [ _read_item_group(read_format_mg) for
                   i in range(rows_per_block) ]

            # skip empty groups
            source.read(size_cg)
            source.read(size_awg)

            mwg = [ _read_item_group(read_format_mwg) for
                    i in range(rows_per_block) ]

            buffer.extend([ list(chain(*block)) for block in zip(tog,mg,mwg) ])

            read_blocks += 1
            buffered_blocks += 1

            # write to database either on buffer break or end of file
            if buffered_blocks >= buffer_blocks or read_blocks == num_blocks:

                insert(connection, table_name, buffer, insert_col_names,
                       schema=schema_name)

                written_rows += len(buffer)

                buffer = []
                buffered_blocks = 0

                # update stdout debug message showing read progress
                if show_debug_msg:
                    msg = debug_buffer_msg.format(read_blocks, num_blocks,
                                                  100 * read_blocks / num_blocks)

                    if len(msg) > out_chars_max: out_chars_max = len(msg)
                    stdout.write('\r' + msg + ' '*max(0,out_chars_max-len(msg)))
                    stdout.flush()

        if show_debug_msg:
            stdout.write("\nFinished reading {} blocks, wrote {} rows\n".\
                format( read_blocks, written_rows))
            stdout.flush()

    return









