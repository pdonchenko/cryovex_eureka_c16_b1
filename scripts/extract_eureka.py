import h5py
from pg_tools import table_exists, drop_table, create_table, insert
from sys import stdout

# DEFAULT SETTINGS
d_config = {
    'overwrite_table' : False,
    'schema_name' : 'public',
    'data_group_key' : "eureka_data",
    'id_name' : {
        'auger' : 'oid_auger',
        'esc30' : 'oid_esc30',
        'magnaprobe' : 'oid_mgn'
    },
    'idType' : 'bigserial PRIMARY KEY',
    'adapt_table_name' : {
        # hdf5 file dataset name : database table name
        'auger' : 'auger_src',
        'esc30' : 'esc30_src',
        'magnaprobe' : 'mgn_src'
    },
    'adapt_col_name' : {
        'auger' : {
            'ice_freboard' : 'ice_freeboard'
        }
    },
    'adapt_col_type' : {
        'float' : ('double precision', lambda v: float(v)),
        'int' : ('integer', lambda v: int(v)),
        'bytes' : ('varchar', lambda v: v.decode("UTF-8"))
    }
}

def extract_eureka(connection, source_path, config=None,
                   show_debug_msg=False):
    """
    Extract eureka ground observations to postgis enables psql database.
    """

    def _adapt_col_name(col_name):
        try:
            return config['adapt_col_name'][dname][cname]
        except KeyError:
            return col_name

    def _adapt_col_type(col_type_name):

        # look for matching type conversion
        for token, (pgtype, converter) in config['adapt_col_type'].items():
            if col.dtype.name.startswith(token):
                return pgtype, converter

        # raise exception if no type could be matched
        if col_type is None:
            raise Exception("HDF5 column '{}' type '{}' could not be"
                            "adapted to a PostgreSQL type".format(
                cname,col.dtype.name
            ))


    # add items to dict from default dict if the user didn't include them
    config = d_config if not config else {**d_config, **config}

    # with h5py.File(source_path) as eureka:

    eureka = h5py.File(source_path)

    group = eureka[config['data_group_key']]

    # common configurations
    schema_name = config['schema_name']

    for dname,dataset in group.items():
        # key must exist in dict
        table_name = config['adapt_table_name'][dname]

        if show_debug_msg:
                    stdout.write("Reading dataset {}\n".format(
                        dname))
                    stdout.flush()

        col_names = []
        col_types = []
        converted = []

        # adapt column names and types based on configuration
        for cname, col in dataset.items():

            col_name = _adapt_col_name(cname)
            col_type, col_converter = _adapt_col_type(col.dtype.name)

            # TODO optimize reading and retyping of source columns
            # problem is that psycopg2 doesn't recognize numpy data types
            # probably not worth improving for such a small dataset

            if len(col.shape) == 1 or col.shape[1] == 1:
                col_names.append(col_name)
                col_types.append(col_type)
                converted.append([ col_converter(i) for i in list(col) ])
            elif col.shape[0] > 1:

                for s in range(col.shape[0]):
                    col_names.append(col_name+str(s+1))
                    col_types.append(col_type)
                    converted.append([ col_converter(i) for i in list(col[s]) ])

            else:
                raise Exception("Unexpected shape {} for column {}".format(
                    col.shape, cname))

        # drop table if it exists and can be dropped
        if table_exists(connection, table_name):
            if config['overwrite_table']:
                drop_table(connection, table_name)

                if show_debug_msg:
                    stdout.write("Dropped table {}.{}\n".format(
                        schema_name, table_name))
                    stdout.flush()

            else:
                raise Exception("Table {}.{} exists but cannot be"
                                "overwritten due to settings".format(
                    schema_name,table_name))

        # create columsn configuration from names and type
        # prepend id columns (not used when inserting)
        cols_config = [(config['id_name'][dname],config['idType'])] \
                     + list(zip(col_names, col_types))

        create_table(connection, table_name, cols_config, schema_name)

        if show_debug_msg:
            stdout.write("Created table {}.{}\n".format(
                schema_name,table_name))
            stdout.flush()

        rows = list(zip(*converted))

        insert(connection, table_name, rows, col_names)

        if show_debug_msg:
            stdout.write("Inserted {} rows x {} columns\n".format(
                len(rows),len(cols_config)))
            stdout.flush()

    eureka.close()

    return



