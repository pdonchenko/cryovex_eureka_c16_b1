"""
Tools for working with PostgreSQL PostGIS enabled database.
Author: Paul Donchenko - University of Waterloo
"""

from contextlib import contextmanager
import psycopg2 as pg
from psycopg2 import sql as pgsql, extras as pgx
from collections.abc import Iterable
import numpy as np

import ec_config as ec

# DEFAULTS
d_schema = ec.d_schema
d_srid = 4326 # WGS-84
d_geom_type = 'POINT'
d_geom_name = 'geom'
d_geom_dim = 2
d_pagesize = 100
d_join_str = ', '

# SQL CONFIGURATION
# name of column that has column names in table schema response
col_info_col = 'column_name'
wildcard = '*'
cursor_colname_index = 0
sql_type_funcs = {
    'identifier' : pgsql.Identifier,
    'I' : pgsql.Identifier,
    'literal' : pgsql.Literal,
    'L' : pgsql.Literal,
    'statement' : pgsql.SQL,
    'S' : pgsql.SQL
}

queries = {
    'check_table_exists' :\
    """
    SELECT EXISTS (
       SELECT 1
       FROM   information_schema.tables 
       WHERE  table_schema = {schema_name}
       AND    table_name = {table_name}
       );
    """,

    'drop_table' :\
    "DROP TABLE {schema}.{table}",

    'create_table' :\
    "CREATE TABLE {schema}.{table} ({col_formats})",

    'get_table_cols_info' :\
    """
    SELECT *
    FROM information_schema.columns
    WHERE table_schema = {schema_name}
    AND table_name = {table_name}
    """,

    'add_geom_col' :\
    """
    SELECT AddGeometryColumn(
        {schema_name},{table_name},{col_name},{srid},{geom_type},{dim})""",

    'calc_geom_col' :\
    """
    UPDATE {schema}.{table}
    SET {geom_col} = ST_SetSRID(ST_MakePoint({lon_col}, {lat_col}), {src_srid})
    """,

        'calc_geom_col_reproj' :\
    """
    UPDATE {schema}.{table}
    SET {geom_col} = ST_Transform(ST_SetSRID(
        ST_MakePoint({lon_col}, {lat_col}), {src_srid}),{tgt_srid})
    """,

        'create_simple_index' : \
    """
    CREATE INDEX ON {schema}.{table} ({col_names})
    """,

        'create_spatial_index' : \
    """
    CREATE INDEX ON {schema}.{table} USING GIST({geom_col})
    """,
    
    'insert' :\
    "INSERT INTO {schema}.{table} ({col_names}) VALUES %s",

    'insert_blank' :\
    "INSERT INTO {schema}.{table} VALUES %s",

    'select' :\
    """
    SELECT {col_names}
    FROM {schema}.{table}
    {where_block}
    {order_block}
    """,

    'drop_col':\
    "ALTER TABLE {schema}.{table} DROP COLUMN {col_name}{cascade_statement}"

}

@contextmanager
def openpg(**kwargs):
    """
    Creates and returns a context manager scope which maintains a database
    connection and closes when the scope ends.
    Usage:
    login = {host:'localhost',user:'postgres',password:'****',dbname:'mydb'}
    with openpg(**login) as connection:
        # connection variable can be used within the scope
        # create a cursor to manipulate data
        cursor = connection.cursor

    # automatically closes connection when scope ends
    """
    connection = pg.connect(**kwargs)
    try:
        yield connection
    finally:
        connection.close()

def _is_non_string_sequence(arg):
    """
    Return whether the argument is a non-string sequence or iterable.
    """

    result = all((
        hasattr(arg, '__iter__'),
        not isinstance(arg, str),
        not isinstance(arg, bytes)
    ))

    return result

def _as_sequence(arg, seq_type=tuple):
    """
    Return the argument as-is if it is a non-string sequence or iterable.
    Otherwise return it is a single-item tuple.
    """

    return arg if _is_non_string_sequence(arg) else seq_type((arg,))

def _build_cols_create_format(cols_config):
    """
    Create a validated composite SQL object which stores a list of name,type
    pairs for defining the structure of table columns in CREATE queries.

    cols_config is a list of pairs (col_name, col_type) where
        col_name is a string specifying the column name
        col_type is a string specifying the column's SQL data type
    """
    col_pairs = [
        (pgsql.Identifier(cname), pgsql.SQL(dtype))
        for cname, dtype in cols_config
    ]

    # should not be () wrapped since psycopg2 auto-wraps column name-type pairs
    # for INSERT statements
    return pgsql.SQL(', ').join(
        ( pgsql.SQL("{} {}").format(*pair) for pair in col_pairs )
    )

def _parse_sql_args(args_dict, sql_join_str=d_join_str):
    """
    Converts a dictionary of arguments to validated pgsql objects ready to be
    inserted into a SQL statement.
    """

    parsed_args = {}

    for atype,args in args_dict.items():
        # apply a different parsing function depending on the type of argument
        parse_func = sql_type_funcs[atype]

        for key,value in args.items():
            # if the value for the key is an iterable and not a string
            # parse join the arguments within the value for this key and
            if _is_non_string_sequence(value):
                parse_iter = ( parse_func(arg) for arg in value )
                parsed_args[key] = pgsql.SQL(sql_join_str).join(parse_iter)
            else:
            # otherwise parse only the single argument in the value
                parsed_args[key] = parse_func(value)

    return parsed_args

def _format_sql(sql_object, args=None, parsed_args=None):
    """
    Format a SQL composable object with arguments.
    """
    if args is None: args = {}
    if parsed_args is None: parsed_args = {}

    return sql_object.format(**parsed_args, **_parse_sql_args(args))

def _format_query(query, args=None, parsed_args=None):
    """
    Format a string SQL query with arguments.
    """

    return _format_sql(pgsql.SQL(query), args, parsed_args)

def _parse_clause_where(*statements, args=None, parsed_args=None):
    """
    Apply SQL type and syntax validation to a set of statements to generate
    a WHERE clause.
    """

    if not statements:
        return pgsql.SQL("")

    block = ""

    # check each statement and merge it into block so it follows SQL syntax
    for s in statements:
        current = s.lower()

        if current.startswith("where"):
            formatted = s[5:]
        elif current.startswith("and"):
            formatted = s[3:]
        else:
            formatted = s

        if formatted.endswith("and"):
            formatted = formatted[:-3]

        if block:
            block += " AND " + formatted
        else:
            block += " WHERE " + formatted

    # apply arguments to block
    if args is None: args = {}
    if parsed_args is None: parsed_args = {}

    return _format_query(block, args, parsed_args)

def _parse_clause(prefix, col_names=None):
    if not col_names:
        return pgsql.SQL("")
    else:
        args = {'I':{'col_names':col_names}}
        return _format_query(prefix+" "+"{col_names}", args)

def _parse_clause_order(col_names=None):
    """
    Apply SQL type and syntax validation to a group of column names to generate
    an ORDER BY clause.
    """

    return _parse_clause("ORDER BY",col_names)

def _parse_clause_group(col_names=None):
    """
    Apply SQL type and syntax validation to a group of column names to generate
    a GROUP BY clause.
    """

    return _parse_clause("GROUP BY",col_names)

def parse_items_to_sql_list(items, sql_type, join_str=", ", template=None):
    """
    TODO - description
    """

    # get the parsing function to use on each item of in group
    func = sql_type_funcs[sql_type]

    # convert the items in the group to the appropriate SQL type
    parsed = [ func(i) for i in items ]

    # apply a template to each item if necessary
    if isinstance(template,str):
        parsed = [ pgsql.SQL(template).format(i) for i in parsed ]

    # join the items together
    return pgsql.SQL(join_str).join(parsed)

def parse_values_list(values_list, sql_types, join_str_in=", ",
                      join_str_out=", ", wrap_str="({})"):
    """
    Converts values_list into a formatted SQL statement of a list of lists
    using sql_types.
    """

    # get the parsing functions to use on each item of a group in values_list
    funcs = [ sql_type_funcs[t] for t in sql_types ]

    # convert the items in each group to the appropriate SQL type
    parsed = [ [ f(i) for f,i in zip(funcs,group) ] for group in values_list ]

    # join the items within each group using the , character
    inside_joined = [ pgsql.SQL(join_str_in).join(group) for group in parsed ]

    # wrap each group in brackets
    wrapped = [ pgsql.SQL(wrap_str).format(group) for group in inside_joined ]

    # join the groups together using the , character
    return pgsql.SQL(join_str_out).join(wrapped)

def parse_col_rename(col_name, display_name=None, args=None, parsed_args=None):
    """
    Converts a column name and a display name into an SELECT item pair and
    a renamed column identifier for additional clauses.

    Returns: (sql identifier pair, renamed identifier)
    """

    col_formatted = _format_query(col_name, args, parsed_args)


    if display_name:
        display_formatted = _format_query(display_name, args, parsed_args)
        pair = pgsql.SQL("{} {}").format(col_formatted, display_formatted)

        return pair, display_formatted

    else:
        return col_formatted, col_formatted


def execute_query(connection, query, args=None, parsed_args=None,
                  commit=True, fetchall=False, description=False,
                  as_cols=False):
    """
    Executes 'query' based on a dictionary of arguments 'args' if provided.
    Arguments must be split into 'identifier', 'literal' and 'statement' dicts
    which contains the actual arguments based on what their role is in the
    query.
    """
    cursor = connection.cursor()

    query_formatted = _format_query(query, args, parsed_args)

    cursor.execute(query_formatted)

    if commit: connection.commit()

    # build return values based on what is requested in context of description
    # and fetchall
    out_params = []

    if description: out_params.append(cursor.description)
    if fetchall:
        if as_cols:
            out_params.append(list(zip(*cursor.fetchall())))
        else:
            out_params.append(cursor.fetchall())

    cursor.close()

    if len(out_params) == 0: return
    elif len(out_params) == 1: return out_params[0]
    else: return tuple(out_params)

def table_exists(connection, table, schema=d_schema):
    """
    Return boolean which indicates whether 'table' exists in 'schema'.
    """

    args = {
        'L' : {'schema_name':schema, 'table_name': table }
    }
    query = queries['check_table_exists']

    response = execute_query(connection, query, args, fetchall=True)

    return response[0][0]

def check_table_exists(connection, table, schema=d_schema):
    """
    Raises exception if table does not exist in schema.
    """

    if not table_exists(connection, table, schema):
        raise Exception("Table {}.{} does not exist".format(schema, table))
    else:
        return True

def drop_table(connection, table, schema=d_schema):
    """
    Drop 'table' from 'schema'.
    """

    check_table_exists(connection, table, schema)

    args = {
        'I' : {'schema':schema, 'table':table}
    }
    query = queries['drop_table']

    execute_query(connection, query, args)

def create_table(connection, table, cols_config, schema=d_schema):
    """
    Create 'table' in 'schema' with columns based on 'cols_config'.
    """
    args = {
        'I' : {'schema':schema, 'table':table}
    }

    # build a validated argument for the column formatting argument
    parsed_args = {'col_formats':_build_cols_create_format(cols_config)}
    query = queries['create_table']

    execute_query(connection, query, args, parsed_args)

def get_table_cols_info(connection, table, schema=d_schema):
    """
    Returns information dict for columns in 'table' in 'schema'.
    """

    check_table_exists(connection, table, schema)

    # does not use execute_query due to special cursor requirement
    cursor = connection.cursor(cursor_factory=pgx.RealDictCursor)

    query = queries['get_table_cols_info']
    args = {'L':{'table_name':table, 'schema_name':schema}}
    query_formatted = _format_query(query, args)

    cursor.execute(query_formatted)
    response = cursor.fetchall()

    cursor.close()
    return response


def get_table_cols_names(connection, table, schema=d_schema):
    """
    Returns names of columns in 'table' in 'schema'.
    """

    check_table_exists(connection, table, schema)

    response = get_table_cols_info(connection, table, schema)

    return [row[col_info_col] for row in response]

def col_in_table(connection, table, col_name, schema=d_schema):
    """
    Returns boolean indicating whether 'col_name' is a column in 'table' in
    'schema'.
    """

    check_table_exists(connection, table, schema)

    return col_name in get_table_cols_names(connection, table, schema)

def add_geom_col(connection, table, col_name=d_geom_name, srid=d_srid,
                 geom_type=d_geom_type, dim=d_geom_dim, schema=d_schema):
    """
    Adds a postgis geometry column to 'table' in 'schema' with name 'col_name'.
    """

    check_table_exists(connection, table, schema)
    
    args = {
        'L' : {'schema_name':schema, 'table_name':table, 'col_name':col_name,
               'srid':srid, 'geom_type':geom_type, 'dim':dim}
    }
    query = queries['add_geom_col']

    execute_query(connection, query, args)

def calc_geom_col(connection, table, lon_col_name, lat_col_name,
                  geom_col=d_geom_name, src_srid=d_srid, tgt_srid=None,
                  schema=d_schema):
    """
    Calculate and update a geometry field in 'table' in 'schema' based on
    existing latitude 'lat_col_name' and longitude 'lon_col_name' columns.
    Can convert spatial reference id (srid) from 'src_srid' to 'tgt_srid'.
    Original lat and lon columns must be in geographic coordinates.
    """
    args = {
        'I' : {'schema':schema, 'table':table, 'lon_col':lon_col_name,
               'lat_col':lat_col_name, 'geom_col':geom_col},
        'L' : {'src_srid':src_srid, 'tgt_srid':tgt_srid}
    }

    # if we are reprojecting from one srid to another then use a special query
    if tgt_srid is None or src_srid == tgt_srid:
        query = queries['calc_geom_col']
    else:
        query = queries['calc_geom_col_reproj']

    execute_query(connection, query, args)

def create_simple_index(connection, table, col_names=None, use_first_cols=1,
                        schema=d_schema):
    """
    Creates a generically named index for 'table' in 'schema' based on columns
    in 'col_names'.
    """

    check_table_exists(connection, table, schema)

    # use the first 'use_first_cols' cols from the target table if
    # no columns names are provided
    if col_names is None:
        temp_cols = get_table_cols_names(connection, table, schema)
        col_names = temp_cols[:min(use_first_cols,len(temp_cols))]

    args = {
        'I' : {'schema':schema, 'table':table, 'col_names':col_names}
    }
    query = queries['create_simple_index']

    execute_query(connection, query, args)

    # not necessary to check for name collision since name is autogenerated


def create_spatial_index(connection, table,
                         geom_col=d_geom_name, schema=d_schema):
    """
    Creates a generically named spatial index for 'table' in 'schema' on
    geometry column based on 'geom_col'
    """

    check_table_exists(connection, table, schema)

    args = {
        'I' : {'schema':schema, 'table':table, 'geom_col':geom_col}
    }
    query = queries['create_spatial_index']

    execute_query(connection, query, args)

    # not necessary to check for name collision since name is autogenerated

def insert(connection, table, rows, col_names=None, template=None,
           template_args=None, schema=d_schema, page_size=d_pagesize):
    """
    Inserts values in 'rows' into 'table' in 'schema' for columns 'col_names'.
    A 'template' can also be specified and takes sql arguments 'template_args'
    """
    cursor = connection.cursor()

    check_table_exists(connection, table, schema)

    # does not use execute_query due to special batch execute function

    # setup table and schema arguments as base for formatting query
    query_args = {
        'I':{'table':table, 'schema':schema},
        'S':{}
    }

    # parse column names if they are provided, otherwise leave them blank
    if col_names:
        query_args['I']['col_names'] = col_names
        query = queries['insert']
    else:
        query = queries['insert_blank']

    # format query with column names as arguments
    query_formatted = _format_query(query, query_args)
    # convert to string to fit into execute_values function
    query_string = query_formatted.as_string(cursor)

    # format template with arguments if they are provided
    if template_args:
        template_formatted = _format_query(template, template_args)
        # convert to string to fit into execute_values function
        template = template_formatted.as_string(cursor)

    pgx.execute_values(cursor, query_string, rows, template, page_size)

    connection.commit()
    cursor.close()

def select(connection, table, select_cols=None, where_clause=None,
           args=None, parsed_args=None, order_cols=None, description=False,
           as_cols=False, schema=d_schema, exclude_cols=None):
    """
    Returns array of rows of cells from columns in 'col_names' from 'table'
    in 'schema'.
    Rows are ordered by columns in 'order_by_cols'.
    """

    check_table_exists(connection, table, schema)

    # arguments for table location
    local_args = {
        'I':{'table':table, 'schema':schema},
        'S':{}
    }

    # add arguments for SELECT columns
    if not select_cols and not exclude_cols:
        local_args['S']['col_names'] = wildcard
    else:
        if not select_cols:
            select_cols = get_table_cols_names(connection, table, schema)

        if exclude_cols:
            select_cols = list(select_cols)
            exclude_cols = _as_sequence(exclude_cols)
            try:
                for col in exclude_cols: select_cols.remove(col)
            except ValueError:
                pass

        local_args['I']['col_names'] = select_cols

    # parse the where clause
    where_clause = _as_sequence(where_clause) if where_clause else ()
    where_parsed = _parse_clause_where(*where_clause, args=args,
                                       parsed_args=parsed_args)

    # parse the order clause
    order_parsed = _parse_clause_order(order_cols)

    query = queries['select']

    if parsed_args is None:
        parsed_args = {}

    parsed_args = {
        **parsed_args,
        **_parse_sql_args(local_args),
        'where_block' : where_parsed,
        'order_block' : order_parsed
    }

    response = execute_query(connection, query, None, parsed_args,
                             fetchall=True, description=description,
                             as_cols=as_cols)

    return response

def read_table_by_xy(connection, table, x_col, y_expressions,
                     where_clause=None, order_cols=None, rename_x=None,
                     args=None, parsed_args=None, schema=d_schema):
    """
    Extract table to a graphable dictionary based on an x column and multiple
    y columns.
    """

    query = \
    """
    SELECT {select_block}
    FROM {schema}.{table}
    {where_block}
    {group_block}
    {order_block}
    """

    qform = lambda q: _format_query(q, args, parsed_args)

    # format the x column using the provided sql arguments
    x_format, x_ref = parse_col_rename(x_col, rename_x, args, parsed_args)

    # create a select statement using the provided columns
    select_formatted = pgsql.SQL(", ").join((
       x_format,
       *[ qform(expr) for expr in y_expressions ]
    ))

    # generic formatting of the where and order clauses
    where_clause = _as_sequence(where_clause)
    where_formatted = _parse_clause_where(*where_clause)
    order_formatted = _parse_clause_order(order_cols)
    # group the table by the x column
    group_formatted = pgsql.SQL("GROUP BY {}").format(x_ref)

    # put together the query clauses
    parsed_args = {
        'select_block' : select_formatted,
        'where_block' : where_formatted,
        'group_block' : group_formatted,
        'order_block' : order_formatted
    }

    table_args = {
        'I' : {'schema':schema, 'table':table}
    }

    # apply the arguments and execute the query
    descr, cols = execute_query(connection, query, table_args, parsed_args,
                                fetchall=True, description=True, as_cols=True)
    # extract variables from the query
    col_names = [ col.name for col in descr ]
    col_data = [ np.array(col) for col in cols ]

    # format to be used when graphing table
    extracted = {
        'x_name' : col_names[0],
        'y_names' : col_names[1:],
        'data' : [ (col_data[0],y_col) for y_col in col_data[1:] ]
    }

    return extracted

def read_table_by_key(connection, table, x_col, key_col, y_expression,
                      where_clause=None, order_cols=None, rename_x=None,
                      rename_key=None, args=None, parsed_args=None,
                      schema=d_schema):
    """
    Extract table to a graphable dictionary based on an x column, a key column
    and a sigle y column.
    """
    query = \
    """
    SELECT {select_block}
    FROM {schema}.{table}
    {where_block}
    {group_block}
    {order_block}
    """

    qform = lambda q: _format_query(q, args, parsed_args)

    # format the x and key columns using the provided sql arguments
    x_format, x_ref = parse_col_rename(x_col, rename_x, args, parsed_args)
    key_format, key_ref = parse_col_rename(key_col, rename_key, args,
                                           parsed_args)

    # create a select statement using the provided columns
    select_formatted = pgsql.SQL(", ").join((
       key_format,x_format,qform(y_expression)
    ))

    # generic formatting of the where and order clauses
    where_clause = _as_sequence(where_clause)
    where_formatted = _parse_clause_where(*where_clause)
    order_formatted = _parse_clause_order(order_cols)
    # group the table by the key and x columns
    group_formatted = pgsql.SQL("GROUP BY {},{}").format(key_ref,x_ref)

    # put together the query clauses
    parsed_args = {
        'select_block' : select_formatted,
        'where_block' : where_formatted,
        'group_block' : group_formatted,
        'order_block' : order_formatted
    }

    table_args = {
        'I' : {'schema':schema, 'table':table}
    }

    # apply the arguments and execute the query
    descr, cols = execute_query(connection, query, table_args, parsed_args,
                                fetchall=True, description=True, as_cols=True)
    # extract variables from the query
    col_data = [ np.array(col) for col in cols ]
    col_names = [ col.name for col in descr ]

    # find the unique elements in the key column so that we can separate
    # the y column into those as categories
    # then use the names in the legend
    unique_y_names = list(np.unique(col_data[0]))

    # for each category in the key, extract the relevant rows for the x and y
    # columns
    data = []
    for y_name in unique_y_names:
        mask = col_data[0] == y_name
        data.append((col_data[1][mask],col_data[2][mask]))

    # format to be used when graphing table
    extracted = {
        'key_name' : col_names[0],
        'x_name' : col_names[1],
        'y_names' : unique_y_names,
        'data' : data
    }

    return extracted

def filter_by_col(connection, table, col_filter, cols_y, num_bins=100,
                  filter_dir='desc', where_clause=None, agr_func="avg",
                  schema=d_schema):
    """
    TODO - description
    """

    query = \
    """
    WITH
        _block AS (
            SELECT {select_cols}
            FROM {schema}.{table}
            {where_block}
        ),
        _stats AS (
            SELECT
                MIN({col_filter})::numeric vmin,
                MAX({col_filter})::numeric vmax,
                ((MAX({col_filter})-MIN({col_filter}))/{num_bins})::numeric vstep
            FROM _block
        ),
        _series AS (
            SELECT bin.*
            FROM _stats
            CROSS JOIN LATERAL generate_series({series_args}) bin
        )
    SELECT bin {prefixed_col_filter}, COUNT({col_filter}),
        {y_avg_block}
    FROM _block,_series
    WHERE {col_filter} {comp_dir} bin
    GROUP BY bin
    ORDER BY bin {order_dir}
    """

    # force columns to be a list
    cols_y = _as_sequence(cols_y)

    # combine filter column with y columns
    select_cols = pgsql.SQL(", ").join(
        ( pgsql.Identifier(i) for i in [col_filter, *cols_y])
    )

    # specify addition to where clause to exclude any rows with nulls
    where_non_null = [ '"{}" IS NOT NULL'.format(name) for name in cols_y ]

    # build where clause and block
    if where_clause is None:
        where_clause = where_non_null
    else:
        where_clause = list(_as_sequence(where_clause)) + where_non_null
    where_block = _parse_clause_where(*where_clause)

    # set arguments based on direction of filtering
    if filter_dir == 'desc':
        series_args = pgsql.SQL("vmin,vmax+vstep,vstep")
        comp_dir = "<="
        order_dir = "DESC"
        prefixed_col_filter = "max_"+col_filter
    elif filter_dir == 'asc':
        series_args = pgsql.SQL("vmax+step,vmin,-vstep")
        comp_dir = ">="
        order_dir = "ASC"
        prefixed_col_filter = "max_"+col_filter
    else:
        raise ValueError("filter_dir must be 'desc' or 'asc'")

    # parse y columns for final select statement
    y_avg_block = pgsql.SQL(",").join((
        pgsql.SQL("{}({}) {}").format(
            pgsql.SQL(agr_func),
            pgsql.Identifier(name),
            pgsql.Identifier(agr_func+"_"+name)
        )
        for name in cols_y)
    )

    # combine all arguments
    parsed_args = dict(
        select_cols=select_cols,
        where_block=where_block,
        series_args=series_args,
        y_avg_block=y_avg_block,
    )
    args = dict(
        identifier=dict(schema=schema,table=table,col_filter=col_filter),
        literal=dict(num_bins=num_bins),
        statement=dict(
            prefixed_col_filter=prefixed_col_filter,
            comp_dir=comp_dir,
            order_dir=order_dir
        )
    )

    # get columns from sql server
    descr, cols = execute_query(connection, query, args, parsed_args,
                                    fetchall=True, description=True,
                                    as_cols=True)
    # extract variables from the query
    col_data = [ np.array(col) for col in cols ]
    col_names = [ col.name for col in descr ]

    # format to be used when graphing table
    extracted = {
        'x_name' : col_names[0],
        'y_names' : col_names[2:],
        'data' : [ (col_data[0],y_col) for y_col in col_data[2:] ],
        'count' : col_data[1]
    }

    return extracted

def drop_col(connection, table, col_name, schema=d_schema, cascade=False):
    """
    Drops a single column 'col_name' from 'table' in 'schema'.
    """

    check_table_exists(connection, table, schema)

    args = {
        'I' : {'table':table, 'schema':schema, 'col_name':col_name},
        'S' : {}
    }

    if cascade:
        args['S']['cascade_statement'] = " CASCADE"
    else:
        args['S']['cascade_statement'] = ""

    query = queries['drop_col']

    execute_query(connection, query, args)


