-- create als_optm table (very slow)
DROP TABLE IF EXISTS als_optm;
CREATE TABLE als_optm AS (
    SELECT DISCINCT als.*
    FROM als_src als
        INNSER JOIN asr_optm asr
        ON st_dwithin(asr.geom, als.geom, 40)
)


-- als within 40m of asiras (very slow)
SELECT DISTINCT als.*
FROM als_src als INNER JOIN asr_optm asr ON st_dwithin(asr.geom, als.geom, 40)


-- create als_optm (faster)
DROP TABLE IF EXISTS als_optm;
CREATE TABLE als_optm AS (
	WITH
		_buffers AS ( SELECT st_buffer(geom, 40) geom FROM asr_optm),
		_area AS (SELECT st_multi(st_union(geom)) geom FROM _buffers)
	SELECT als.*
	FROM als_src AS als JOIN _area
	ON st_intersects(als.geom, _area.geom)
)


-- create mgn_optm (faster)
DROP TABLE IF EXISTS mgn_optm;
CREATE TABLE mgn_optm AS (
	WITH
		_buffers AS ( SELECT st_buffer(geom, 40) geom FROM asr_optm),
		_area AS (SELECT st_multi(st_union(geom)) geom FROM _buffers)
	SELECT mgn.*
	FROM mgn_src AS mgn JOIN _area
	ON st_intersects(mgn.geom, _area.geom)
)

-- asiras aggregate mgn - multirange
WITH
	_buffer AS (SELECT oid, st_buffer(geom, 40) geom FROM asr_optm),
	_ranges AS (SELECT generate_series(1,40) rng)
SELECT _buffer.oid, COUNT(*) "count", _ranges.rng "range", AVG(mgn.snow_depth) "mean"
FROM _buffer, asr_optm AS asr, mgn_src AS mgn, _ranges
WHERE _buffer.oid = asr.oid
AND st_contains(_buffer.geom, mgn.geom)
AND st_dwithin(asr.geom, mgn.geom, _ranges.rng)
GROUP BY _buffer.oid, "range"

-- create ice surface elevation
DROP TABLE IF EXISTS ise_optm;
CREATE TABLE ise_optm AS (
	SELECT
		_source.oid,
		(SELECT _target.snow_elvtn FROM als_optm AS _target ORDER BY _source.geom <-> _target.geom LIMIT 1) -_source.snow_depth AS ice_elvtn,
		_source.geom geom
	FROM mgn_optm AS _source
)

-- combining aggregate tables
CREATE TABLE asr_aggr_all AS (
	WITH _dr AS (SELECT generate_series(2,40,2) "dist")
	SELECT
		asr.oid, _dr."dist",
		mgn."count" mgn_count, mgn."mean" mgn_mean, mgn."min" mgn_min,
			mgn."max" mgn_max, mgn."range" mgn_range, mgn."var" mgn_var,
			mgn."asperity_cont" mgn_asperity_cont, mgn."asperity_disc" mgn_asperity_disc,
		als."count" als_count, als."mean" als_mean, als."min" als_min,
			als."max" als_max, als."range" als_range, als."var" als_var,
			als."asperity_cont" als_asperity_cont, als."asperity_disc" als_asperity_disc,
		isr."count" isr_count, isr."mean" isr_mean, isr."min" isr_min,
			isr."max" isr_max, isr."range" isr_range, isr."var" isr_var,
			isr."asperity_cont" isr_asperity_cont, isr."asperity_disc" isr_asperity_disc,
		ise."count" ise_count, ise."mean" ise_mean, ise."min" ise_min,
			ise."max" ise_max, ise."range" ise_range, ise."var" ise_var,
			ise."asperity_cont" ise_asperity_cont, ise."asperity_disc" ise_asperity_disc
		
	FROM asr_optm asr, asr_agr_mgn mgn, asr_agr_als als, asr_agr_isr isr, asr_agr_ise ise, _dr
	WHERE asr.oid = mgn.oid AND asr.oid = als.oid AND asr.oid = isr.oid AND asr.oid = ise.oid
	AND _dr."dist" = mgn.dist_range AND _dr."dist" = als.dist_range AND _dr."dist" = isr.dist_range AND _dr."dist" = ise.dist_range
)

-- combining footprint aggregate tables with NULLS
CREATE TABLE asr_ftagr_all AS (
	SELECT *
	FROM asr_optm
		FULL OUTER JOIN asr_ftagr_mgn ON oid = mgn_oid
		FULL OUTER JOIN asr_ftagr_als ON oid = als_oid
		FULL OUTER JOIN asr_ftagr_ise ON oid = ise_oid
		FULL OUTER JOIN asr_ftagr_isr ON oid = isr_oid
)

-- combining aggregate tables with NULLS
CREATE TABLE asr_agr_all AS (
	WITH asr AS (
			SELECT asr_optm.oid, generate_series(2,40,2) "dist" FROM asr_optm
		)
	SELECT *
	FROM asr
		FULL OUTER JOIN asr_agr_mgn ON oid = mgn_oid AND dist = mgn_dist
		FULL OUTER JOIN asr_agr_als ON oid = als_oid AND dist = als_dist
		FULL OUTER JOIN asr_agr_ise ON oid = ise_oid AND dist = ise_dist
		FULL OUTER JOIN asr_agr_isr ON oid = isr_oid AND dist = isr_dist
)

-- calculate error table
WITH sp AS(
	SELECT
		oid, dist,
		als_mean - 3.80 "snow_elvtn",
		als_mean - 3.80 - mgn_mean "ice_elvtn_spread",
		ise_mean - 3.80 "ice_elvtn_linked",
		mgn_mean "snow_depth"
	FROM asr_agr_all
	WHERE mgn_mean IS NOT NULL AND als_mean IS NOT NULL AND ise_mean IS NOT NULL
)
SELECT
	oid, dist,
	tfmra_40 - ice_elvtn_spread "error_spread",
	@(tfmra_40 - ice_elvtn_spread) "abs_error_spread",
	1-((tfmra_40 - ice_elvtn_spread)/snow_depth) "penetration_spread",
	(tfmra_40 - ice_elvtn_spread)/snow_depth "norm_error_spread",
	@((tfmra_40 - ice_elvtn_spread)/snow_depth) "abs_norm_error_spread",
	tfmra_40 - ice_elvtn_linked "error_linked",
	@(tfmra_40 - ice_elvtn_linked) "abs_error_linked",
	1-((tfmra_40 - ice_elvtn_linked)/snow_depth) "penetration_linked",
	(tfmra_40 - ice_elvtn_linked)/snow_depth "norm_error_linked",
	@((tfmra_40 - ice_elvtn_linked)/snow_depth) "abs_norm_error_linked",
	tfmra_40 > snow_elvtn "above_snow",
	tfmra_40 < ice_elvtn_spread "below_ice_spread",
	tfmra_40 < ice_elvtn_linked "below_ice_linked",
	tfmra_40 >= ice_elvtn_spread AND tfmra_40 <= snow_elvtn "within_pack_spread",
	tfmra_40 >= ice_elvtn_linked AND tfmra_40 <= snow_elvtn "within_pack_linked"
FROM asr_wvfm_char wvfm JOIN sp USING (oid)

-- combining all features
WITH
	asr AS (
		SELECT oid, roll, pitch, yaw, ocog_width
		FROM asr_src
	),
	wvfm AS (
		SELECT oid, tfmra_40, rwidth_full, rwidth_lead, rwidth_trail,
			pp_full, pp_left, pp_right
		FROM asr_wvfm_char
	),
	agr AS (
		SELECT oid, dist, mgn_count, als_count, mgn_mean, mgn_range, mgn_var,
			als_mean, als_range, als_var, als_asperity_disc, als_asperity_cont,
			isr_max, ise_mean, ise_range, ise_var, ise_asperity_cont, ise_asperity_disc
		FROM asr_agr_all
	)
SELECT *
FROM error_dist error
	JOIN asr USING (oid) JOIN wvfm USING (oid) JOIN asr_snow_dens USING (oid)
	JOIN agr USING(oid, dist)
ORDER BY (oid, dist)

-- Nearest ISR point to each ASIRAS nadir
SELECT _nearest.*
FROM asr_optm asr
JOIN LATERAL (
	SELECT asr.oid, isr.roughness
	FROM isr_optm isr
	ORDER BY asr.geom <-> isr.geom
	LIMIT 1
	) AS _nearest USING (oid)
	
-- Generic get n nearest features

SELECT _nearest.*
FROM src_table src
JOIN LATERAL (
	SELECT src.id, tgt.val
	FROM tgt_table tgt
	ORDER BY src.geom <-> tgt.geom
	LIMIT 1
	) AS _nearest USING (id)
	
	
-- Distance-weighted avg of nearest two snow densities for each asr
WITH
	_pairs AS (
		SELECT asr.oid, _near.snow_dens, st_distance(asr.geom, _near.geom) "dist"
		FROM asr_src asr
			JOIN LATERAL (
				SELECT asr.oid, (snow_rho1+snow_rho2)/2 "snow_dens", dens.geom
				FROM esc30_src dens
				ORDER BY asr.geom <-> dens.geom LIMIT 2
			) AS _near USING (oid)
		ORDER BY asr.oid
	),
	_dist_sums AS (
		SELECT oid, SUM(dist) "dist_sum"
		FROM _pairs GROUP BY oid
		ORDER BY oid
	),
	_applied AS (
		SELECT oid, snow_dens*(dist/dist_sum) "snow_dens", dist
		FROM _pairs JOIN _dist_sums USING (oid)
	)
SELECT oid, AVG(snow_dens) "snow_dens", MIN(dist) "dist_near", MAX(dist) "dist_far"
FROM _applied
GROUP BY oid ORDER BY oid

-- TEMPORARY

SELECT *
FROM mgn_optm mgn
JOIN LATERAL (
	SELECT mgn.oid, als.snow_elvtn, als.geom
	FROM als_optm als
	ORDER BY mgn.geom <-> als.geom
	LIMIT 1
	) AS _near USING (oid)
	
	
-- Artefact slow version of mgn to als ice surface query
CREATE TABLE {output} AS (
	SELECT
		_source.oid,
		(
		    SELECT _target.snow_elvtn
		    FROM {als_optm} AS _target
		    ORDER BY _source.geom <-> _target.geom
		    LIMIT 1
		) -_source.snow_depth AS ice_elvtn,
		_source.geom geom
	FROM {mgn_optm} AS _source
)

-- analyze selected offsets

WITH
	_offsets AS (
		SELECT * FROM ( VALUES {values_list} ) s("name","offset")
	),
	_surf AS (
		SELECT oid_asr, als_mean snow_elvtn, mgn_mean snow_depth,
			ise_mean ice_elvtn
		FROM {ftagr_all}
		WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
	),
	_sensor AS (
		SELECT oid_asr, "name", "offset", {elvtn_field}+"offset" est_elvtn
		FROM {asr_wvfm_char}, _offsets
	)
SELECT "name", "offset",
	SUM((est_elvtn>snow_elvtn)::integer) "above_snow_spread",
	SUM((est_elvtn>(ice_elvtn+snow_depth))::integer) "above_snow_linked",
	SUM((est_elvtn<(snow_elvtn-snow_depth))::integer) "below_ice_spread",
	SUM((est_elvtn<(ice_elvtn))::integer) "below_ice_linked",
	|/(SUM(((est_elvtn-(snow_elvtn-snow_depth))^2))/COUNT(*)) "rmse_spread",
	|/(SUM(((est_elvtn-ice_elvtn)^2))/COUNT(*)) "rmse_linked",
	CORR(est_elvtn,(snow_elvtn-snow_depth)) "corr_spread",
	CORR(est_elvtn,ice_elvtn) "corr_linked"
	
FROM _sensor JOIN _surf USING (oid_asr)
GROUP BY "name","offset"
ORDER BY "rmse_spread"

-- Nearest ALS to each MGN
SELECT _nearest.*, st_distance(src.geom, _nearest.geom)
FROM mgn_optm src
JOIN LATERAL (
	SELECT src.oid, tgt.snow_elvtn, tgt.geom
	FROM als_src tgt
	ORDER BY src.geom <-> tgt.geom
	LIMIT 1
	) AS _nearest USING (oid)
	
-- More complex (final) version of ALS to each MGN
SELECT oid, mgn.snow_depth, _near.snow_elvtn "als_elvtn",
	_near.snow_elvtn-mgn.snow_depth "ice_elvtn",
	st_distance(mgn.geom, _near.geom) "als_dist", mgn.geom
FROM mgn_src mgn
JOIN LATERAL (
	SELECT mgn.oid, als.snow_elvtn, als.geom
	FROM als_src als
	ORDER BY mgn.geom <-> als.geom
	LIMIT 1
	) AS _near USING (oid)
	
	
-- semivariance (of mgn) using buffers around evenly selected asiras nadirs in the first half of the track
WITH
	_gap AS ( SELECT FLOOR(COUNT(*)/2)::integer FROM asr_src ),
	_steps AS ( SELECT generate_series(0, (SELECT * FROM _gap), (SELECT * FROM _gap)/10) oid_asr ),
	_zones AS ( SELECT oid_asr, st_buffer(geom, 60) geom FROM _steps JOIN asr_src USING(oid_asr) ),
	_within AS (
		SELECT oid_asr, oid_mgn oid_obsv, obsv.geom, snow_depth val
		FROM _zones JOIN mgn_src obsv ON st_within(obsv.geom, _zones.geom)
		),
	_pairs AS (
		SELECT a.oid_asr, (a.val-b.val)^2 dsqr, st_distance(a.geom, b.geom) dist
		FROM _within a JOIN _within b ON a.oid_asr = b.oid_asr AND a.oid_obsv <> b.oid_obsv
	),
	_grouped AS (
		SELECT SUM(dsqr)/(2*COUNT(dsqr)) semivar, ROUND(dist/1) dist_bin
		FROM _pairs GROUP BY dist_bin ORDER BY dist_bin
	)
SELECT * FROM _grouped

-- buffered semivariance for ALS (~5 min)
DROP TABLE IF EXISTS semivar_als;
CREATE TABLE semivar_als AS (
	WITH
		_gap AS ( SELECT FLOOR(COUNT(*)/2)::integer FROM asr_src ),
		_steps AS ( SELECT generate_series(0, (SELECT * FROM _gap), (SELECT * FROM _gap)/10) oid_asr ),
		_zones AS ( SELECT oid_asr, st_buffer(geom, 60) geom FROM _steps JOIN asr_src USING(oid_asr) ),
		_within AS (
			SELECT oid_asr, oid_als oid_obsv, obsv.geom, snow_elvtn val
			FROM _zones JOIN als_src obsv ON st_within(obsv.geom, _zones.geom)
			),
		_pairs AS (
			SELECT a.oid_asr, (a.val-b.val)^2 dsqr, st_distance(a.geom, b.geom) dist
			FROM _within a JOIN _within b ON a.oid_asr = b.oid_asr AND a.oid_obsv <> b.oid_obsv
		),
		_grouped AS (
			SELECT SUM(dsqr)/(2*COUNT(dsqr)) semivar, ROUND(dist/1) dist_bin
			FROM _pairs GROUP BY dist_bin ORDER BY dist_bin
		)
	SELECT * FROM _grouped
)


-- semivariance for full mgn track
DROP TABLE IF EXISTS semivar_mgn;
CREATE TABLE semivar_mgn AS (
	WITH
		_pairs AS (
			SELECT st_distance(a.geom, b.geom) dist, (a.snow_depth-b.snow_depth)^2 diff_sqr
			FROM mgn_src a JOIN mgn_src b ON a.oid_mgn <> b.oid_mgn
		)
	SELECT ROUND(dist/1) dist_bin, SUM(diff_sqr)/(2*COUNT(diff_sqr)) semivar
	FROM _pairs
	GROUP BY dist_bin
)

-- combine footprint with distance aggregation
WITH
	_leftcols AS ( SELECT oid_asr, -1 dist FROM ftagr_all ),
	_combined AS ( SELECT * FROM _leftcols JOIN ftagr_all USING (oid_asr) )
SELECT * FROM _combined
UNION
SELECT * FROM agr_all
ORDER BY dist, oid_asr

-- calculate observed ice surface elevations
(
WITH agr AS (SELECT * FROM agr_full)
SELECT
	oid_asr, dist, 'spread' surf_calc, als_mean snow_elvtn,
	mgn_mean snow_depth, als_mean-mgn_mean ice_elvtn
FROM agr
WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
UNION
SELECT
	oid_asr, dist, 'linked' surf_calc, als_mean snow_elvtn,
	mgn_mean snow_depth, ise_mean ice_elvtn
FROM agr
WHERE als_mean IS NOT NULL AND mgn_mean IS NOT NULL
AND ise_mean IS NOT NULL
)
ORDER BY oid_asr,dist,surf_calc

-- calculate complex error
WITH
	_offsets AS (
		SELECT *
		FROM ( VALUES 
			('optimal',3.29),('best_high_med',3.310),('best_high',3.322),
			('best',3.335),('march_26',3.80) )
			s(offset_name, offset_val)
		),
	_sensor AS (
		SELECT oid_asr, tfmra_level, offset_name, offset_val,
			tfmra_elvtn+offset_val adj_elvtn
		FROM asr_elvtn, _offsets
	)
SELECT
	oid_asr, dist, surf_calc, tfmra_level, offset_name, offset_val, adj_elvtn,
	adj_elvtn-ice_elvtn error, @(adj_elvtn-ice_elvtn) abs_error,
	(snow_elvtn-adj_elvtn)/snow_depth penetration,
	(adj_elvtn-ice_elvtn)/snow_depth norm_error,
	@((adj_elvtn-ice_elvtn)/snow_depth) abs_norm_error,
	(adj_elvtn>snow_elvtn)::integer above_snow,
	(adj_elvtn<ice_elvtn)::integer below_ice,
	(adj_elvtn<=snow_elvtn AND adj_elvtn >= ice_elvtn)::integer in_pack
FROM agr_surf JOIN _sensor USING(oid_asr)

-- isr footprint aggregation
WITH
	_near AS (
		SELECT _local_near.*
		FROM asr_optm asr
		JOIN LATERAL (
			SELECT asr.oid_asr, isr.roughness ice_rough_near
			FROM isr_optm isr
			ORDER BY asr.geom <-> isr.geom
			LIMIT 1
			) AS _local_near USING (oid_asr)
	),
	_optm_ftpr AS (
        SELECT ftpr.*
        FROM asr_ftpr ftpr, asr_optm optm
        WHERE ftpr.oid_asr = optm.oid_asr
    ),
    _ftagr AS (
		SELECT ftpr.oid_asr, MIN(roughness) ice_rough_min,
			MAX(roughness) ice_rough_max,
			ROUND(AVG(roughness),4) ice_rough_perc
		FROM isr_optm isr, _optm_ftpr ftpr
		WHERE st_contains(ftpr.geom, isr.geom)
		GROUP BY ftpr.oid_asr
	)
SELECT * FROM _ftagr FULL OUTER JOIN _near USING (oid_asr) ORDER BY oid_asr

-- asr distance aggregation
WITH
	_near AS (
		SELECT _local_near.*
		FROM asr_optm asr
		JOIN LATERAL (
			SELECT asr.oid_asr, isr.roughness ice_rough_near
			FROM isr_optm isr
			ORDER BY asr.geom <-> isr.geom
			LIMIT 1
			) AS _local_near USING (oid_asr)
	),
	_dists AS (
		SELECT generate_series(2,40,2) dist
	),
	_agr AS (
		SELECT nadir.oid_asr, dist, MIN(roughness) ice_rough_min,
			MAX(roughness) ice_rough_max, ROUND(AVG(roughness),4) ice_rough_perc
		FROM isr_optm isr, asr_optm nadir, asr_optm_bufr bufr, _dists
		WHERE nadir.oid_asr = bufr.oid_asr
			AND st_contains(bufr.geom, isr.geom)
			AND st_dwithin(nadir.geom, isr.geom, dist)
		GROUP BY nadir.oid_asr, dist
	),
	_test AS ( SELECT * FROM _agr JOIN _near USING (oid_asr) )
SELECT oid_asr, ice_rough_min, ice_rough_max, ice_rough_perc,
	ice_rough_near
FROM _test
WHERE dist = (SELECT MIN(dist) FROM _test)

	
-- old snow density query
CREATE TABLE {output} AS (
WITH
	_pairs AS (
		SELECT asr.oid_asr, _near.snow_dens,
		    st_distance(asr.geom, _near.geom) "dist"
		FROM {asr_src} asr
			JOIN LATERAL (
				SELECT asr.oid_asr, (snow_rho1+snow_rho2)/2 "snow_dens",
				    dens.geom
				FROM {esc30_src} dens
				ORDER BY asr.geom <-> dens.geom LIMIT 2
			) AS _near USING (oid_asr)
		ORDER BY asr.oid_asr
	),
	_dist_sums AS (
		SELECT oid_asr, SUM(dist) "dist_sum"
		FROM _pairs GROUP BY oid_asr
		ORDER BY oid_asr
	),
	_interp AS (
		SELECT oid_asr, snow_dens*(dist/dist_sum) "snow_dens", dist
		FROM _pairs JOIN _dist_sums USING (oid_asr)
	)
SELECT oid_asr, AVG(snow_dens) "snow_dens_interp",
    
    MIN(dist) "dist_near",
    MAX(dist) "dist_far"
FROM _applied
GROUP BY oid_asr ORDER BY oid_asr
)

-- filtering query
WITH
	_block AS (
		SELECT
			spread_error,
			als_asperity_disc
		FROM
			comb_full
		WHERE
			dist = 12
			AND spread_error IS NOT NULL
			AND ice_rough_max IS NOT NULL
	),
	_stats AS (
		SELECT
			MIN(als_asperity_disc)::numeric vmin,
			MAX(als_asperity_disc)::numeric vmax,
			((MAX(als_asperity_disc)-MIN(als_asperity_disc))/200)::numeric vstep
		FROM _block
	),
	_series AS (
		SELECT bin.*
		FROM _stats
		CROSS JOIN LATERAL generate_series(vmin,vmax+vstep,vstep) bin
	)
SELECT bin, COUNT(als_asperity_disc), AVG(spread_error) spread_error
FROM _block,_series
WHERE als_asperity_disc < bin
GROUP BY bin
ORDER BY bin DESC