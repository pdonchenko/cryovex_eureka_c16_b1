"""
Build cross correlation matrix of all signal and surface observed
characteristics.
Author: Paul Donchenko - University of Waterloo
"""

import ec_config as ec
import ec_tools as et
import pg_tools as pt

with pt.openpg(**ec.pg_login) as conn:

    print("Cross correlation of footprint range")
    path_ft = r"..\results\corrmatrix\corr matrix footprint.xlsx"
    et.cross_corr_table_to_xlsx(conn, path_ft, ec.comb_char, -1)


    print("Cross correlation of 12 meter range")
    path_12m = r"..\results\corrmatrix\corr matrix 12m.xlsx"
    et.cross_corr_table_to_xlsx(conn, path_12m, ec.comb_char, 12)

print("done step analyze corr")

