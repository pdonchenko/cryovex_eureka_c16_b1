"""
Use shallow snow zones to determine a potentially more accurate offset between
ALS and ASIRAS
Author: Paul Donchenko - University of Waterloo
"""

import ec_config as ec
import ec_tools as et
import pg_tools as pt

from matplotlib import pyplot as plt

with pt.openpg(**ec.pg_login) as conn:

    # TFMRA threshold to use for analyzing offsets
    tfmra_threshold = 0.4

    # Calculate ALS-ASIRAS calibration offsets using selected records that:
    #   1. have shallow snow, to minimize snow layering interference
    #   2. are in a zone of observation-confirmed flat ice
    #       this reduces the effects of large off-nadir ice features
    #   3. the radar footprint intersects with snow-depth observation
    #       that cover the full spread of the footprint
    #       (i.e. are not concentrated on one side)
    targets_path = r"..\data\calibration\asiras grid targets.csv"
    shallow_offsets = et.calib_shallow_snow(conn, targets_path,
                                            tfmra_threshold)

    # Plot RMSE over a range of offsets to see the RMSE changes as the offset
    # is changed
    # Calculates an optimal offset for both spread and linked ice elevation
    rmse_optimal_offsets = et.analyze_offset_range(conn, 3, 4, 0.01,
                                                   tfmra_threshold)

    # Offsets from documentation
    doc_offsets = {
        'march_26' : 3.84,
        'march_26' : 3.80
    }

    # Combine all the offsets
    offsets = {**doc_offsets, **shallow_offsets, **rmse_optimal_offsets}

    # Analyze the number of out-of-snowpack returns for each offset
    analysis = et.analyze_offset_list(conn, offsets, tfmra_threshold)
    print("STATS FOR SELECTED OFFSETS:")
    print(analysis)


    # Offsets used in calc error step
    print("RESULT OFFSET PAIRS")
    print(list(offsets.items()))

plt.show()
print("done step analyze offset")