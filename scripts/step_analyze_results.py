"""
Use module graphing support to analyze the relationships between variables.
"""

import ec_config as ec
import ec_tools as et
import pg_tools as pt
from matplotlib import pyplot as plt
with pt.openpg(**ec.pg_login) as conn:

    if True:

        # Analyzing variables over distance range by offset
        where_clause = ("surf_calc='spread'","tfmra_level=0.4","dist>0")
        table = ec.error_full
        x_col = 'dist'
        key_col = 'offset_name'

        # Penetration over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(penetration)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Penetration over distance range by offset",
                       xlabel="Aggregation Distance Range (m)",
                       ylabel="Penetration (prop.)")

        # Error over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(error)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Error over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Error (m)")

        # Absolute error over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(abs_error)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Absolute error over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Absolute Error (m)")

        # Normalized error over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(norm_error)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Normalized error over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Absolute Error (m)")

        # Absolute normalized error over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(abs_norm_error)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Absolute normalized error over distance range by "
                             "offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Absolute Error (m)")

        # Records below ice over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(below_ice)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Below ice over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Proportion records below ice surface")

        # Records above snow over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(above_snow)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Above snow over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Proportion records above snow surface")

        # Records within snow pack over distance range by offset
        result = pt.read_table_by_key(conn, table, x_col, key_col,
                                      'AVG(in_pack)', where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Within snowpack over distance range by offset",
                       xlabel="Aggregation Distance (m)",
                       ylabel="Proportion records within snowpack")

    if True:

        # Filter variables to show how the change if some records are removed
        where_clause = "dist=12"
        table = ec.comb_char
        # NOTE: comb_char is already limited

        # Filter over snow surface roughness
        filter_col = 'als_asperity_cont'
        # Filter error
        y_cols = ('spread_error','spread_abs_error',
                  'linked_error','linked_abs_error')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Error filtered over snow surface roughness",
                       xlabel="Max snow surface roughness index",
                       ylabel="Error (m)")

        # Filter penetration
        y_cols = ('spread_penetration','linked_penetration')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Penetration filtered over snow surface roughness",
                       xlabel="Max snow surface roughness index",
                       ylabel="Penetration (prop.)")

        # Filter normalized error
        y_cols = ('spread_norm_error','spread_abs_norm_error',
                  'linked_norm_error','linked_abs_norm_error')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Snow depth-normalized error filtered over snow "
                             "surface roughness",
                       xlabel="Max snow surface roughness index",
                       ylabel="Error (m per m of snow)")

        # Filter snowpack indicators error
        y_cols = ('spread_above_snow','spread_below_ice','spread_in_pack',
                  'linked_above_snow','linked_below_ice','linked_in_pack')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Estimated ice surface location relative to snow "
                             "pack over snow surface roughness"
                             "surface roughness",
                       xlabel="Max snow surface roughness index",
                       ylabel="Proportion of records meeting criteria")

        # Filter snow depth
        y_cols = ('mgn_mean',)
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Snow depth over snow surface roughness",
                       xlabel="Max snow surface roughness index",
                       ylabel="Snow depth (m)")


        # Filter over OCOG width
        filter_col = 'ocog_width'
        # Filter error
        y_cols = ('spread_error','spread_abs_error',
                  'linked_error','linked_abs_error')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Error filtered over OCOG width",
                       xlabel="Max OCOG width index",
                       ylabel="Error (m)")

        # Filter penetration
        y_cols = ('spread_penetration','linked_penetration')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Penetration filtered over OCOG width",
                       xlabel="Max OCOG width index",
                       ylabel="Penetration (prop.)")

        # Filter normalized error
        y_cols = ('spread_norm_error','spread_abs_norm_error',
                  'linked_norm_error','linked_abs_norm_error')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Snow depth-normalized error filtered over snow "
                             "surface roughness",
                       xlabel="Max OCOG width index",
                       ylabel="Error (m per m of snow)")

        # Filter snowpack indicators error
        y_cols = ('spread_above_snow','spread_below_ice','spread_in_pack',
                  'linked_above_snow','linked_below_ice','linked_in_pack')
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Estimated ice surface location relative to snow "
                             "pack over OCOG width"
                             "surface roughness",
                       xlabel="Max OCOG width index",
                       ylabel="Proportion of records meeting criteria")

        # Filter snow depth
        y_cols = ('mgn_mean',)
        result = pt.filter_by_col(conn, table, filter_col, y_cols,
                                  where_clause=where_clause)
        plt.figure()
        axes = plt.axes()
        et.result_plot(result, axes,
                       title="Snow depth over OCOG width",
                       xlabel="Max OCOG width index",
                       ylabel="Snow depth (m)")

    # Display results in figures
    plt.show()