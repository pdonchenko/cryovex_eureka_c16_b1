import ec_config as ec
import ec_tools as et
import pg_tools as pt

# open connection to postgis server
with pt.openpg(**ec.pg_login) as conn:

    # select offsets based on calibration analysis
    offset_pairs = (
        [('march_23', 3.84), ('march_26', 3.8), ('best', 3.735470076358683),
         ('best_high', 3.7218281173200527),
         ('best_high_med', 3.7103450504991318),
         ('optimal_rmse_spread', 3.69),
         ('optimal_rmse_linked', 3.69)]
    )

    # CALCULATE ERROR
    print("calculating error")
    # full error for all characteristic combinations
    et.calc_error_full(conn, ec.error_full, offset_pairs)

    # COMBINING CHARACTERISTICS
    print("combining characteristics")
    et.combine_char(conn, ec.comb_char, 0.4, 'spread', 'march_26')

    # print("calculating characteristics tables")
    # # combine distance aggregated characteristics
    # et.combine_char_dist(conn, ec.comb_dist)
    # # combine footprint aggregated characteristics
    # et.combine_char_footprint(conn, ec.comb_ft)

    print("done step calc error")

