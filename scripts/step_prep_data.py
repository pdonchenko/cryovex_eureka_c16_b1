"""
Import data for analysis and prepare it for calibration.
1. Import source data
2. Filter out potentially erroneous and unprocessable data
3. Create datasets by combining imported data
4. Aggregate observations to each ASIRAS nadir using the radar altimetry
    footprint and a series of distances from the nadir
5. Calculate characteristics of the signal return for each waveform

Author: Paul Donchenko - University of Waterloo
"""

import ec_config as ec
import ec_tools as et
import pg_tools as pt

# open connection to postgis server
with pt.openpg(**ec.pg_login) as conn:

    # IMPORT SOURCE DATA
    print("importing source data")
    # import ASIRAS radat altimetry to postgis database
    et.import_asiras(conn, ec.asr_src, ec.asr_path)
    # import ALS laser altimetry
    et.import_als(conn, ec.als_src, ec.als_path)
    # import Eureka campaign ground observations
    et.import_eureka(conn, ec.erk_src, ec.erk_path)


    # CREATE OPTIMAL TRACK
    print("calculating optimal track")
    # create optimal ASIRAS table
    et.create_asr_optm(conn, ec.asr_optm)
    # create buffers around optimal asiras points
    et.create_asr_optm_bufr(conn, ec.asr_optm_bufr)
    # create table of mgn points filtered to proximity near optm asiras
    et.create_optm_obsv(conn, 'magnaprobe', ec.mgn_optm, ec.mgn_src)
    # create table of als points filtered to proximity near optm asiras
    et.create_optm_obsv(conn, 'ALS', ec.als_optm, ec.als_src)


    # CREATE INTERMEDIATE DATASETS
    print("calculating intermediate datasets ")
    # create ASIRAS snow density reference
    et.create_snow_dens(conn, ec.asr_snow_dens)
    # create linked ice surface elevation (ISE) nearest ALS to each magnaprobe
    et.create_ice_surf_elvtn(conn, ec.ise)

    # CALCULATE WAVEFORM CHARACTERISTICS
    print("calculating waveform characteristics")
    tfmra_thresholds = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    et.calc_wvfm_tables(conn, ec.asr_elvtn, ec.asr_char, tfmra_thresholds)

    # SPATIAL AGGREGATION OVER DISTANCE RANGES
    print("calculating spatial aggregation over distances")
    # aggregate magnaprobe snow depth over ASIRAS nadirs
    et.create_dist_agr(conn, 'magnaprobe', 'mgn', ec.agr_mgn, ec.mgn_optm,
                       ec.valcol_mgn)
    # aggregate als snow surface elevation over ASIRAS nadirs (very long)
    et.create_dist_agr(conn, 'ALS', 'als', ec.agr_als, ec.als_optm,
                       ec.valcol_als)
    # aggregate ise ice surface elevation over ASIRAS nadirs
    et.create_dist_agr(conn, 'ice surface elevation', 'ise', ec.agr_ise,
                       ec.ise, ec.valcol_ise)
    # aggregate ice surface roughness (ISR) to each ASIRAS nadir
    et.create_dist_agr_isr(conn, ec.agr_isr)
    # combine distance aggregated tables into one
    et.combine_agr_dist(conn, ec.agr_all)


    # SPATIAL AGGREGATION WITHIN FOOTPRINTS
    print("calculating spatial aggregation over footprints")
    # aggregate magnaprobe snow depth within ASIRAS footprints
    et.create_ft_agr(conn, 'magnaprobe', 'mgn', ec.ftagr_mgn,
                     ec.mgn_optm, ec.valcol_mgn)
    # aggregate als snow surface elevation within ASIRAS footprints
    et.create_ft_agr(conn, 'ALS', 'als', ec.ftagr_als, ec.als_optm,
                     ec.valcol_als)
    # aggregate ise ice surface elevation within ASIRAS footprints
    et.create_ft_agr(conn, 'ice surface elevation', 'ise', ec.ftagr_ise,
                     ec.ise, ec.valcol_ise)
    # aggregate ice surface roughness (ISR) within ASIRAS footprints
    et.create_ft_agr_isr(conn, ec.ftagr_isr)
    # combine footprint aggregated tables into one
    et.combine_ft_agr(conn, ec.ftagr_all)


    # COMBINE AGGREGATION TABLES AND CALCULATE OBSERVED ICE SURFACE
    print("combining aggregation tables and calculating observed ice surface")
    et.combine_agr_full(conn, ec.agr_full)
    et.calc_agr_surface(conn, ec.agr_surf)

    print("done step prep data")











