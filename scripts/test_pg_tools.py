"""
Simple testing of tool sripts. Only covers very basic cases.
"""

import pg_tools as pt
from psycopg2 import sql as pgsql
import ec_config as ec

with pt.openpg(**ec.pg_login) as C:
    # PGTOOLS

    # creating, dropping and checking table
    table = "test_table"
    schema = "public"

    if pt.table_exists(C, table, schema):
        pt.drop_table(C, table, schema)

    cols_config = (('tid','bigint'),('values','double precision'))

    pt.create_table(C, table, cols_config, schema)
    assert pt.table_exists(C, table, schema)

    pt.drop_table(C, table, schema)
    assert not pt.table_exists(C, table, schema)

    # inserting and selecting rows
    rows = [(1,2.1),(2,2.2),(3,2.3),(4,2.4)]

    pt.create_table(C, table, cols_config, schema)
    pt.insert(C, table, rows, schema=schema)

    result = pt.select(C, table, schema=schema)
    assert result == rows

    expected = [ (i[1],) for i in rows ]
    result =  pt.select(C, table, 'values', schema=schema)
    assert result == expected

    # creating simple index
    pt.create_simple_index(C, table, 'tid', schema)

    # getting row information
    assert pt.col_in_table(C, table, 'values', schema)
    assert not pt.col_in_table(C, table, 'notacol', schema)

    pt.drop_table(C, table, schema)

    # working with geometry
    cols_config = (('tid', 'bigserial PRIMARY KEY'),('lat','double precision'),
                ('lon', 'double precision'))
    rows = [(1,2),(3,4),(5,6),(7,8),(9,10),(11,12)]

    pt.create_table(C, table, cols_config, schema)

    assert pt.table_exists(C, table, schema)

    expected = [ i[0] for i in cols_config ]
    result = pt.get_table_cols_names(C, table, schema)
    assert result == expected

    pt.insert(C, table, rows, ('lon', 'lat'), schema=schema)

    # check complex select
    select_cols = ('tid','lat')
    where_clause = ("{lon_col} > {lon_threshold}","lat < 11")
    args = {
        'I' : { 'lon_col':'lon'}
    }
    parsed_args = {'lon_threshold':pgsql.Literal(2)}
    expected = [(2, 4.0), (3, 6.0), (4, 8.0), (5, 10.0)]
    result = pt.select(C, table, select_cols , where_clause, args,
                       parsed_args, None, exclude_cols=('lon',))
    assert result == expected

    # checking column-specified insert and serial id incrementation
    expected = [ (i+1,) for i in range(len(rows)) ]
    result = pt.select(C, table, 'tid', order_cols='tid', schema=schema)
    assert result == expected

    # add geometry column
    pt.add_geom_col(C, table, 'geom', 4326, schema=schema)
    assert pt.col_in_table(C, table, 'geom', schema)

    pt.calc_geom_col(C, table, 'lon', 'lat', 'geom', 4326)

    # drop columns
    pt.drop_col(C, table, 'lon', schema)
    pt.drop_col(C, table, 'lat', schema)

    assert not pt.col_in_table(C, table, 'lon', schema)
    assert not pt.col_in_table(C, table, 'lat', schema)

    pt.drop_table(C, table)

    # parsing functions
    cursor = C.cursor()

    # parsing format when creating columns
    cols_config = (('foo','integer'),('bar','double precision'))
    expected = '"foo" integer, "bar" double precision'
    result = pt._build_cols_create_format(cols_config).as_string(cursor)
    assert result == expected

    cursor.close()

    # parsing arguments into VALUES list
    values_list = [('foo',1),('bar',2)]
    sql_types = ('literal','literal')
    parsed_values = pt.parse_values_list(values_list, sql_types)
    query = "SELECT * FROM ( VALUES {values_list} ) s(someval) "
    parsed_args = {'values_list':parsed_values}
    expected = values_list
    result = pt.execute_query(C, query, parsed_args=parsed_args,
                              fetchall=True)
    assert result == expected

